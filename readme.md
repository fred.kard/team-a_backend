<div align="center">
  
  <a><img src="images/Logo Backend A.png" alt="Backend A Team" width="400" class="center"></a>
  <h3 align="center">Apps Group - DSC Corporate PIC Apps Version</h3>
  <p align="center">
    <a href="#key-features">Key Features</a> •
    <a href="#credits">Credits</a> •
  </p>
</div>


## Key Features

* Menu for PIC Corporate
  - Can Add New Numbers
  - Can Change Packages for Numbers
  - Can View Make Ticket
  - View Bills per Month
  - Live Chat
  - Email Notification Payment and Add Package
* Menu for AM Telkomsel
  - View Profit
  - View Ticket
  - View Profile Corporate
  - Live Chat
  - Approve Package for Corporate Number
  - Email Notification Payment


## Credits

* Backend Pagi
  - Asep Jajang Nurjaya
  - Dona Saputra
  - Fajar Wahyu Ardianto
* Backend Siang
  - Fajri Rahmanda
  - Stephanus Krisna Arizona Widiputra
  - Handoko Darmawan Wicaksono 


## Mockup

<p align="center">

  <a>
    <img src="images/Mockup.png">
  </a>
</p>

## Table Relation
<p align="center">

  <a>
    <img src="images/Table relation.png">
  </a>
</p>

<h1 align="center">PIC CORPORATE</h1>

## API Login PIC Corporate

![api_login](/uploads/ebed5c4f9bfcf89207ae0793fcd0dbf1/api_login.gif)

## API Edit Profile PIC Corporate

![api_editprofilepic](/uploads/d62d8e4ac85cf74bf44c4ccbdf534649/api_editprofilepic.gif)

## API Change Package PIC Corporate

![api_changepackagepic](/uploads/261e9660566825c5147f202cc25c9c80/api_changepackagepic.gif)

## API Get Order For PIC Corporate

![api_getorderpic](/uploads/41bbdae08b4e9c5940e03d4da787028b/api_getorderpic.gif)

## API Get Order Items For PIC

![api_getorderitemsforpic](/uploads/5d6200c19c73c9b036cff5512bdbca49/api_getorderitemsforpic.gif)

## API Create Order Add Numbers

![api_createOrderAddNumbers](/uploads/ace17ba78b219533054fc849cad7dffa/api_createOrderAddNumbers.gif)

## API Create Ticket

![api_createticket](/uploads/d842fd47428fd4c795d8fcc6ed578f95/api_createticket.gif)

## API Ticket User

![api_ticketuser](/uploads/51e34f3cb949b5ed9cffe0e74dd876a5/api_ticketuser.gif)

## API My Ticket Detail PIC Corporate

![api_myticketdetailpic](/uploads/cfdc78852fea849fe9639334dd3a7f95/api_myticketdetailpic.gif)

## API Reply Ticket PIC Corporate

![api_replyticketpic](/uploads/2da3cb9adef6ef7fcb1308a233abe49b/api_replyticketpic.gif)

## API Close Ticket

![api_closeticket](/uploads/e8ee536a95e11d1be8aa8dd55ae519d7/api_closeticket.gif)

## API Get Current Package Summary

![api_getCurrentPackageSummary](/uploads/5f5121bc36d7aabd71115c0f45c1433a/api_getCurrentPackageSummary.gif)

## API Get Bill Month

![api_getBillMonth](/uploads/9571e6cbde64215f3ba3d43cbab5f73f/api_getBillMonth.gif)

## API Get Bill Detail

![api_getBillDetail](/uploads/1b8a4bd7b0834d303109916cd48e1c17/api_getBillDetail.gif)

## API PIC Corporate Billing

![api_piccorpbilling](/uploads/56252ad49d8821c065cb8b28f04aff72/api_piccorpbilling.gif)

## API Unpaid Corporate Billing

![api_unpaidcorpbilling](/uploads/f66894d9355340b7fc1e796b06752a9f/api_unpaidcorpbilling.gif)

## API Update Payment

![api_updatePayment](/uploads/0af5f7fcd3ff3481df0993d562a33d4f/api_updatePayment.gif)

## API Get MSISDN 

![api_getMsisdn](/uploads/a3ab43b2e5440a5d71d79d7b1df0cc44/api_getMsisdn.gif)


<h1 align="center">AM TELKOMSEL</h1>

## API Login AM Telkomsel

![api_login_AM](/uploads/f6994463ff5fc7b20a02b91e08da0e77/api_login_AM.gif)

## API Edit Profile AM Telkomsel

![api_edit_profile_AM](/uploads/0187b2d44dcee44854cf00365d02529c/api_edit_profile_AM.gif)

## API My Ticket AM Telkomsel

![api_my_ticket](/uploads/1215a4b37eae245f20657a4decfd1928/api_my_ticket.gif)

## API My Ticket Detail

![api_my_ticket_detail](/uploads/dd9a696850e149ce6b0d9723af6fbd4d/api_my_ticket_detail.gif)

## API Get Order For AM Telkomsel

![api_get_order_for_AM](/uploads/36e84fe23d6ba707a782c50ada29aac0/api_get_order_for_AM.gif)

## API Get Order Items For AM Telkomsel

![api_get_order_Items_For_AM](/uploads/d2e5da8c94d88b1820f709250b9b7281/api_get_order_Items_For_AM.gif)

## API Get MSISDN

![api_get_MSISDN](/uploads/61f34e937a77dbfe6016073d5f7aeff8/api_get_MSISDN.gif)

## API Add MSISDN BY AM Telkomsel

![api_Add_MSISDN_By_AM](/uploads/a80092bd64f91facabd514d906f69786/api_Add_MSISDN_By_AM.gif)

## API Change Package

![api_Change_Package](/uploads/dff45e147d4ed4746ef4544b949b7e3a/api_Change_Package.gif)

## API My Corporate 

![api_My_Corporate](/uploads/2ecd41161d9633cfd763fb14d21d339d/api_My_Corporate.gif)

## API Bill Corporate

![api_bill_corporate](/uploads/be6068d348378b7ac91a60b66bae78a6/api_bill_corporate.gif)

## API Get Bill Detail

![api_get_Bill_Detail](/uploads/95007d547063f0c42f80cd04430d2b86/api_get_Bill_Detail.gif)

## API RAM Corporate Billing

![api_ram_corp_billing](/uploads/aacdd565cd9c4635be22fd3d7eaf7e99/api_ram_corp_billing.gif)

## API RAM Unpaid Corporate Billing

![api_RAM_Unpaid_Corp_Billing](/uploads/31a6ea071768a5c40f9bdec16338ed75/api_RAM_Unpaid_Corp_Billing.gif)

## API Profit History

![api_Profit_History](/uploads/ddd143a0481e3c7058914b35a912c234/api_Profit_History.gif)

## API Change Status Order

![api_Change_Status_Order](/uploads/e4e20b12aa697eadcb25090fb41b7d8a/api_Change_Status_Order.gif)

## API Change Status Order Items

![api_Change_Status_Order_Item](/uploads/47d1febcfbe4e3ee92b55080100f9418/api_Change_Status_Order_Item.gif)

## API Reply Ticket AM

![api_reply_ticket_AM](/uploads/8d44f43331115ea169b2e80e250aaee3/api_reply_ticket_AM.gif)
