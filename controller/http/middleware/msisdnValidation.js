module.exports = {
    msisdnValidator (userMsisdn) {
    
        //check msisdn is number!
        var misdnInt = Number(userMsisdn)
        if(isNaN(misdnInt)) {
            return `msisdn not a number`
        }
        //check 1st number have to be started by "0" || "6"
        if(userMsisdn[0] > 0 &&  userMsisdn[0] < 6 || userMsisdn[0] > 6 ) {
            return `please check your first number`
        }
        //if starts by "0" then replaced to "62" before added
        if (userMsisdn[0]==0) {
            var userMsisdnZero = []
            for (let i = 0; i < userMsisdn.length; i++) {
                userMsisdnZero[i] = userMsisdn[i]
            }
            userMsisdnZero.splice(0,1,"6","2")
            var userMsisdnZeroSix = ""
            for (let i = 0; i < userMsisdnZero.length; i++) {
                userMsisdnZeroSix = userMsisdnZeroSix + userMsisdnZero[i]
            }
        }
        //variable become 1 either starts by "0" or "62"
        var userMsisdnSix = ""
        if(userMsisdn[0]==0) {
            var userMsisdnSix = userMsisdnZeroSix
        } else if (userMsisdn[0]==6) {
            var userMsisdnSix = userMsisdn
        }
        //check 2nd is "2"
        if( userMsisdnSix[1] !== "2" ) {
            return `wrong country code`
        }
        //check 3rd is "8"
        if( userMsisdnSix[2] !== "8" ) {
            return `not a phone number`
        }
        //check count of minimum and maximum length of number
        console.log(`jumlah angka ${userMsisdnSix.length}`)
        if( userMsisdnSix.length > 14 || userMsisdnSix.length < 11) {
            return 'check your length number'
        }
        //check msisdn is Telkomsel number that start by (62811 || 62812 || 62813)
        const digitTigaEmpat = userMsisdnSix[3]+userMsisdnSix[4]
        if( digitTigaEmpat == "11" || digitTigaEmpat == "12" || digitTigaEmpat == "13"  ) {
        } else {
            return `not a Telkomsel msisdn`
        }
        return userMsisdnSix     
    }
}
