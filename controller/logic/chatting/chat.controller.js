var fire = require('../../../firestore/fire')
var db = fire.firestore()
const { account_manager } = require('../../../models');
const { pic_corporate_account } = require('../../../models');
db.settings({
    timestampsInSnapshots: true
})

class chatController {
    async sendChat(req, res, next) {
        try {
            const text = req.body
            const { id_account } = req.query
            console.log(id_account)
            var email_am = req.email
            console.log(email_am.email)
            function myFunction(b) {
                var patt = /@telkomsel/i;
                return patt.test(b)
            }
            if (myFunction(email_am.email)){
                const id_am = await account_manager.findAll({
                    where: {
                        email: email_am.email
                    }
                })
                db.collection('chatting').doc(id_account).collection('MESSAGES').add({
                    text,
                    createdAt: new Date(),
                    user: {
                        name: id_am[0].dataValues.name,
                        email: email_am.email
                    }
                })
                res.status(200).json({
                    status: true,
                    message: "chat berhasil di submit",
                    data: {
                        "message": req.body.text,
                        "name": id_am[0].dataValues.name,
                        "email": email_am.email,
                        "createdAt": new Date().getTime()
                    }
                })
                next()
                return

            } else{
                const id_am = await pic_corporate_account.findAll({
                    where: {
                        email: email_am.email
                    }
                })
                db.collection('chatting').doc(id_account).collection('MESSAGES').add({
                    text,
                    createdAt: new Date(),
                    user: {
                        name: id_am[0].dataValues.name,
                        email: email_am.email
                    }
                })
                res.status(200).json({
                    status: true,
                    message: "chat berhasil di submit",
                    data: {
                        "message": req.body.text,
                        "name": id_am[0].dataValues.name,
                        "email": email_am.email,
                        "createdAt": new Date().getTime()
                    }
                })
                next()
                return

            }


        }
        catch (error) {
            console.log(error)
            res.status(500).json({
                status: false,
                message: error.message,
                data: null
            })
            // const bodyRes = {
            //     status: false,
            //     message: "Failed to submit",
            //     data: null
            // }
            // res.send(bodyRes)
            return
        }
    }
}

module.exports = chatController