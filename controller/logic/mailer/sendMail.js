const fs = require('fs');
const nodemailer = require('nodemailer');
const ejs = require('ejs');

class Mailer{
    constructor (pathFile, recipient, subject, data) {
        this.pathFile = pathFile
        this.recipient = recipient,
        this.subject = subject,
        this.data = data 
    }

    async send() {
        let transporter = nodemailer.createTransport({
            host: "smtp.mailtrap.io",
            port: 2525,
            auth: {
              user: "b1949a54f63c19",
              pass: "eb905e8b58de3f"
            }
          });
        
        const sender =`no-reply@dsccorpservice.telkomsel.com.id`
        const data = await ejs.renderFile(this.pathFile , { data : this.data});

        const mainOptions = {
          from: sender,
          to: this.recipient,
          subject: this.subject,
          html: data
        };
        
        transporter.sendMail(mainOptions, (err, info) => {
          if (err) {
            console.log(err);
          } else {
            console.log('Message sent: ' + info.response);
          }
        });
    }
}

module.exports = Mailer;