const jwt = require('jsonwebtoken');
const { pic_corporate_account } = require('../../../models')
const { account_manager } = require('../../../models')
const {
    hashSync,
    genSaltSync,
    compareSync
} = require("bcrypt");

class AuthController {
    //feature untuk authentikasi oleh AM corporate (unused karena perubahan fitur baru)
    async authAM(req,res,next) {
        console.log('masuk am')
        const { email, password } = req.body
        console.log(email)
        await account_manager.findAll(
            {
                where: {
                    email : email,
                }
            }
        )
        .then(async data => {
            if (data.length > 0) {
                const passdb = data[0].dataValues.password
                console.log(passdb)
                const isPasswordValid = await compareSync(password, passdb);
                console.log('selesai compare')
                if (!isPasswordValid) {
                    res.status(401).json({
                        status: false,
                        message: 'Wrong Password',
                        data: null,
                    })
                    next()
                    return
                }

                const token = jwt.sign({
                    email: email,

                }, process.env.SECRET, {});
                var foto = data[0].dataValues.avatar
                const buffer = Buffer.from(foto);
                res.status(200).json({
                    "status": true,
                    "message": "Attempt Authentication Success",
                    "data": {
                        "token": token,
                        "name": data[0].dataValues.name,
                        "pic_id": data[0].dataValues.pic_id,
                        "nik": data[0].dataValues.nik,
                        "email": data[0].dataValues.email,
                        "password": data[0].dataValues.password,
                        "phone": data[0].dataValues.phone,
                        "job_position": data[0].dataValues.job_position,
                        "workplace": data[0].dataValues.workplace,
                       "avatar": buffer.toString()
                    }
                })
                next()
                return
            } else {
                console.log('gagal login')
                req.user_id = 0
                res.status(401).json({
                    "status": false,
                    "message": "Sorry, wrong username or password",
                    "data": null
                })
                next()
                return
            }
        }
        ).catch(async err => {
            res.status(500).json({
                "status": false,
                "message": err.message,
                "data": null
            })
            next()
            return
        }
        )
    }
    //feature untuk authentikasi oleh PIC corporate (unused karena perubahan fitur baru)
    async authCorporate(req, res, next) {
        console.log('masuk auth')
        const { email, password } = req.body
        console.log(email)
        await pic_corporate_account.findAll(
            {
                where:
                {
                    email: email,
                }
            }
        ).then(async data => {
            if (data.length > 0) {
                const passdb = data[0].dataValues.password
                console.log(passdb)
                const isPasswordValid = await compareSync(password, passdb);
                console.log('selesai compare')
                if (!isPasswordValid) {
                    res.status(401).json({
                        status: false,
                        message: 'Wrong Password',
                        data: null,
                    })
                    next()
                    return
                }

                const token = jwt.sign({
                    email: email,

                }, process.env.SECRET, {});
                var foto = data[0].dataValues.avatar
                const buffer = Buffer.from(foto);
                res.status(200).json({
                    "status": true,
                    "message": "Attempt Authentication Success",
                    "data": {
                        "token": token,
                        "name": data[0].dataValues.name,
                        "pic_id": data[0].dataValues.pic_id,
                        "nik": data[0].dataValues.nik,
                        "email": data[0].dataValues.email,
                        "password": data[0].dataValues.password,
                        "phone": data[0].dataValues.phone,
                        "job_position": data[0].dataValues.job_position,
                        "workplace": data[0].dataValues.workplace,
                       "avatar": buffer.toString()
                    }
                })
                next()
                return
            } else {
                console.log('gagal login')
                req.user_id = 0
                res.status(401).json({
                    "status": false,
                    "message": "Sorry, wrong username or password",
                    "data": null
                })
                next()
                return
            }
        }
        ).catch(async err => {
            res.status(500).json({
                "status": false,
                "message": err.message,
                "data": null
            })
            next()
            return
        }
        )

    }
    //New feature untuk login all in one
    async auth(req, res, next){
        const { email, password } = req.body

        //pengecekan domain email
        function myFunction(b) {
            var patt = /@telkomsel/i;
            return patt.test(b)
        }
        if (myFunction(email)) {

            //domain telkomsel
            req.choice = 'telkomsel'
            await account_manager.findAll(
                {
                    where: {
                        email: email,
                    }
                }
            )
                .then(async data => {
                    if (data.length > 0) {
                        const passdb = data[0].dataValues.password
                        console.log(passdb)
                        const isPasswordValid = await compareSync(password, passdb);
                        console.log('selesai compare')
                        if (!isPasswordValid) {
                            res.status(401).json({
                                status: false,
                                message: 'Wrong Password',
                                data: null,
                            })
                            next()
                            return
                        }

                        const token = jwt.sign({
                            email: email,

                        }, process.env.SECRET, {});
                        var foto = data[0].dataValues.avatar
                        const buffer = Buffer.from(foto);
                        res.status(200).json({
                            "status": true,
                            "message": "Attempt Authentication Success",
                            "data": {
                                "token": token,
                                "name": data[0].dataValues.name,
                                "am_id": data[0].dataValues.am_id,
                                "role": "telkomsel",
                                "nik": data[0].dataValues.nik,
                                "email": data[0].dataValues.email,
                                "password": data[0].dataValues.password,
                                "phone": data[0].dataValues.phone,
                                "job_position": data[0].dataValues.job_position,
                                "workplace": data[0].dataValues.workplace,
                                "avatar": buffer.toString()
                            }
                        })
                        next()
                        return
                    } else {
                        console.log('gagal login')
                        req.user_id = 0
                        res.status(401).json({
                            "status": false,
                            "message": "Sorry, wrong username or password",
                            "data": null
                        })
                        next()
                        return
                    }
                }
                ).catch(async err => {
                    res.status(500).json({
                        "status": false,
                        "message": err.message,
                        "data": null
                    })
                    next()
                    return
                }
                )

        } else {

            //domain corporate
            req.choice = 'other'
            await pic_corporate_account.findAll(
                {
                    where:
                    {
                        email: email,
                    }
                }
            ).then(async data => {
                if (data.length > 0) {
                    const passdb = data[0].dataValues.password
                    const isPasswordValid = await compareSync(password, passdb);
                    if (!isPasswordValid) {
                        res.status(401).json({
                            status: false,
                            message: 'Wrong Password',
                            data: null,
                        })
                        next()
                        return
                    }

                    const token = jwt.sign({
                        email: email,

                    }, process.env.SECRET, {});
                    var foto = data[0].dataValues.avatar
                    const buffer = Buffer.from(foto);
                    res.status(200).json({
                        "status": true,
                        "message": "Attempt Authentication Success",
                        "data": {
                            "token": token,
                            "name": data[0].dataValues.name,
                            "pic_id": data[0].dataValues.pic_id,
                            "role": "corporate",
                            "nik": data[0].dataValues.nik,
                            "email": data[0].dataValues.email,
                            "password": data[0].dataValues.password,
                            "phone": data[0].dataValues.phone,
                            "job_position": data[0].dataValues.job_position,
                            "workplace": data[0].dataValues.workplace,
                            "avatar": buffer.toString()
                        }
                    })
                    next()
                    return
                } else {
                    console.log('gagal login')
                    req.user_id = 0
                    res.status(401).json({
                        "status": false,
                        "message": "Sorry, wrong username or password",
                        "data": null
                    })
                    next()
                    return
                }
            }
            ).catch(async err => {
                res.status(500).json({
                    "status": false,
                    "message": err.message,
                    "data": null
                })
                next()
                return
            }
            )
        }
    }
}

module.exports = AuthController