const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const { billing } = require('../../../models');
const Mailer = require('../mailer/sendMail');



class paymentController{
    async updatePayment(req, res, next) {
        const postdata = req.body
        if(!postdata.billing_id || postdata.billing_id == "") {
            res.status(400).json({
                status: false,
                message: "Missing billing_id Parameter Info !"
            })
            return
        }
        try {
            const findBill = await billing.findOne({
                where:{
                    billing_id: postdata.billing_id
                }
            })
            if (findBill) {
                if (findBill.status_payment == "PAID") {
                    res.status(422).json({
                        status: false,
                        message: "Billing Already Paid",
                        data: findBill
                    })
                    return
                }
                try{
                    const user_corporate = req.email
                    const pic = await pic_corporate_account.findOne({
                        where:{
                            email: user_corporate.email
                        }
                    })
                    const corporate = await corporate_account.findOne({
                        where: {
                            pic_id: pic.pic_id,
                        }
                    })
                    if (pic && corporate) {
                        try { 
                            
                            const updatePayment = await billing.update({
                                status_payment: "PAID",
                                total_outstanding: 0
                            }, {where: {
                                account_id: corporate.account_id,
                                billing_id: postdata.billing_id
                            }}
                            )
                            try{
                                if  (updatePayment == 1) {
                                    const data = await billing.findOne({
                                        where:{
                                            billing_id: postdata.billing_id
                                        }
                                    })

                                    //send email notification
                                    let pathFile = './views/emailNotif.ejs'
                                    let picName = pic.name
                                    let emailRecipient = pic.email
                                    let billingId = data.billing_id
                                    let amount = new Intl.NumberFormat('id-ID', {
                                        style: 'currency',
                                        currency: 'IDR',
                                    }).format(data.total_invoice); 
                                    let messageInfo = 'Payment'         
                                    const emailNotif = new Mailer(pathFile,emailRecipient,'Payment Notification', 
                                    {
                                        billingId, 
                                        picName, 
                                        amount,
                                        messageInfo
                                    })
                                    emailNotif.send()
                                    //send email end

                                    res.status(201).json({
                                        status: true,
                                        message: 'Billing Updated',
                                        data
                                    })
                                }
                                else {
                                    res.status(422).json({
                                        status: false,
                                        message: 'updated failed',
                                        data: null,
                                    })
                                }
                            }
                            catch(err) {
                                res.status(500).json({
                                    status: false,
                                    message: err.message
                                })
                            }
                        }
                        catch(err) {
                            res.status(500).json({
                                status: false,
                                message: err.message
                            })
                        }
                    }
                    else {
                        res.status(422).json({
                            status: false,
                            message: 'data reference for billing not found',
                            data: null,
                        })
                    }
                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }
}

module.exports = paymentController