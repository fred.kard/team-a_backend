const { msisdn, corporate_account, account_manager, pic_corporate_account, msisdn_package, packages, order } = require('../../../models');
const {Op} = require('sequelize');



class MsisdnPicController {
    async getMsisdnPic(req,res,next) {
        try {
            const reqAccountId = req.query
            if(reqAccountId === undefined || reqAccountId == "") {
                res.status(422).json({
                    status: false,
                    message: `Falied, check your input`,
                    data: null
                })
            }

            const dataCorpAccount = await corporate_account.findOne({where : {account_id: reqAccountId.account_id} })
            //error handling account id not found
            if(!dataCorpAccount) {
                res.status(422).json({
                    status: false,
                    message: `Falied, account id ${reqAccountId.account_id} not found`,
                    data: null,
                })
            }
            const dataCorp = dataCorpAccount.dataValues

            
            const userActive = req.email;
            let userPic = await pic_corporate_account.findOne({where: {email: userActive.email} })
            if(userPic.dataValues.pic_id != dataCorp.pic_id) {
                res.status(422).json({
                    status: false,
                    message: `Falied, account id ${reqAccountId.account_id} not belong to you`,
                    data: null,
                })
            }
            
            let data = await msisdn.findAll({
                attributes: ['msisdn'],
                where: {
                    [Op.and]: [
                        {account_id: reqAccountId.account_id},
                        {is_active: true}
                    ]
                },
                include: {
                    model: msisdn_package,
                    as: 'msisdn_has_packages',
                    is_active: true,
                    include: {
                        model: packages,
                        as: 'package_in_relasi_packages'
                    }
                }
            })
            let countMsisdn = data.length

            function joinPackages(length,i){
                var listPackage = [] 
                for (let j=0; j<length; j++) {
                    listPackage.push({
                        'package_id': data[i].dataValues.msisdn_has_packages[j].package_id,
                        'package_name': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_name,
                        'package_type': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_type,
                        'package_value': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_value
                    })
                }
                return listPackage
            }
            
            let listMsisdnAndPackage = []

            for(let i = 0; i < countMsisdn; i++) {
                let length = data[i].dataValues.msisdn_has_packages.length
                listMsisdnAndPackage.push({
                    'msisdn': data[i].dataValues.msisdn,
                    'packages': joinPackages(length,i)
                })
            }
            res.status(200).json ({
                status: true,
                message: `Success! get all number of ${dataCorp.account_name} account id ${reqAccountId.account_id}`,
                data: listMsisdnAndPackage
            })
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: error.message,
                data: error
            })
            
        }
    }
}
module.exports = MsisdnPicController