const pic_corporate_account = require('../../../models').pic_corporate_account;
const corporate_account = require('../../../models').corporate_account;
const db = require('../../../models/index');
const { QueryTypes } = require('sequelize');


class billMonthController{
    //feature untuk mengecek tagihan per bulan
    async searchBill(req,res,next) {
        try{
            //pengecekan database dari email yang telah di input (JWT Middleware)
            const user_corporate = req.email

            //pengecekan dari table pic_corporate_account
            const pic = await pic_corporate_account.findOne({
                where:{
                    email: user_corporate.email
                }
            })

            //pengecekan dari table corporate_account
            const corporate = await corporate_account.findOne({
                where: {
                    pic_id: pic.pic_id,
                }
            })

            //query dari table billing
            const queryGetBill = ("SELECT billings.billing_id,billings.month, billings.year, billings.status_payment, billings.total_invoice, billings.total_outstanding, billings.status_payment, billings.virtual_account from billings WHERE billings.account_id = '"+corporate.account_id+"';")
            const myData = await db.sequelize.query(queryGetBill, { type: QueryTypes.SELECT })
            console.log(myData)

                res.status(200).json({
                    status: true,
                    message: "Data retrieved",
                    data: myData
                })
            }
            catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
    }}
    }



module.exports = billMonthController

