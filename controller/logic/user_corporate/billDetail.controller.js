const billing_detail = require('../../../models').billing_detail;

class billDetail{
    //feature untuk mengecek tagihan dengan detail
    async detailBilling(req,res,next) {
        try{
            //konstanta dari req.body
            const {billing_id} = req.body
            console.log(req.body)

            //pengecekan database dari table billing_detail
            await billing_detail.findAll({
                where: {
                    billing_id : billing_id
                }
            })
            .then(async data => {
                var listBill = []
                var index = data.length - 1
                
                //proses pengambilan data dengan Looping for
                for (index; index >= 0;  --index){
                    listBill[index] = {
                        "billing_id" : data[index].dataValues.billing_id,
                        "package_id" : data[index].dataValues.package_id,
                        "package_qty" : data[index].dataValues.package_qty,
                        "package_total" : data[index].dataValues.package_total
                    }
                }
                res.status(200).json({
                    status: true,
                    message: "Attempt Success",
                    data: listBill
                })
                console.log(data)
            })
        }
        catch(error) {
            console.log(error)
            const bodyRes = {
                status: false,
                message: "Failed to submit",
                data: null
            }
            res.send(bodyRes)
            return
        }
    }
}


module.exports = billDetail