const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const db = require('../../../models/index');
const { QueryTypes } = require('sequelize');


class CurrentpackageSummaryControllerPIC{
    async currentPackageSummary(req, res, next) {
        try{
            const user_corporate = req.email
            const pic = await pic_corporate_account.findOne({
                where:{
                    email: user_corporate.email
                }
            })
            const corporate = await corporate_account.findOne({
                where: {
                    pic_id: pic.pic_id,
                }
            })
            console.log(corporate.account_id)
            if (pic && corporate) {
                try {
                    let currentDate = new Date();
                    let currentMonth = currentDate.getMonth() + 1 ;
                    let nextMonth = currentDate.getMonth() + 2 ;
                    let currentYear = currentDate.getFullYear() ;

                    currentMonth = (currentMonth < 10 ? '0' : '') + currentMonth;
                    nextMonth = (nextMonth < 10 ? '0' : '') + nextMonth;

                    let startdate = currentYear + "-" + nextMonth + "-" + "01";
                    let expiredate = currentYear + "-" + currentMonth + "-" + "01";
                    let queryGetCurrentPackage = ("select msisdn_packages.package_id,packages.package_name, count(msisdn_packages.package_id) as package_qty, sum(packages.price) as total from msisdn_packages left join packages on msisdn_packages.package_id = packages.package_id left join msisdns on msisdn_packages.msisdn = msisdns.msisdn left join corporate_accounts on msisdns.account_id = corporate_accounts.account_id where msisdn_packages.start_date < '"+ startdate +"' and msisdn_packages.expire_date >= '"+ expiredate +"' and corporate_accounts.account_id = '"+ corporate.account_id +"' group by msisdn_packages.package_id,packages.package_name;")
                    
                    const data = await db.sequelize.query(queryGetCurrentPackage, { type: QueryTypes.SELECT });
                    if ( data.length == 0 ) {
                        res.status(422).json({
                            status: false,
                            message: 'no current packages active',
                            data: null,
                        })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: `current packages retrieved`,
                            data
                        })
                    }
                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
            }
            else {
                res.status(422).json({
                    status: false,
                    message: 'data not found',
                    data: null,
                })
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }
}

module.exports = CurrentpackageSummaryControllerPIC