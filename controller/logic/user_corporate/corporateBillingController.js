const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const { billing } = require('../../../models');
const sequelize = require('sequelize')


class CorporateBillingController {

    async billingList (req, res) {
    
        try{
            const userActive = req.email;
        
            const foundPicCorpAcc = await pic_corporate_account.findOne({
                where:{
                    email: userActive.email
                }
            })
            if (foundPicCorpAcc !== null){
                const foundCorpAccount = await corporate_account.findAll({  
                    where :{
                        pic_id:foundPicCorpAcc.pic_id
                    },
                    raw: true,
                    nest: true,
            })
            let totalBilling= []
            
            for (let i = 0; i < foundCorpAccount.length; i++)
            if(foundCorpAccount[i].length !== 0) {
                totalBilling[i] = await billing.findAll({
                    attributes: [
    
                      'account_id',
                      [sequelize.fn('sum', sequelize.col('total_invoice')), 'total_invoice'],
                      [sequelize.fn('sum', sequelize.col('total_outstanding')), 'total_outstanding'],
                                                   
                    ],
                    group: ['account_id'],
                    where :{
                        account_id:foundCorpAccount[i].account_id
                        },
                    raw: true,
                    nest: true,
                    
                  });
            }
            let dataOutput =[]
            for (let i = 0; i < totalBilling.length; i++){
                for (let j = 0; j < totalBilling[i].length; j++) {
                        dataOutput[i] = {
                        account_name : foundCorpAccount[i].account_name,
                        account_id : totalBilling[i][j].account_id,
                        total_invoice : totalBilling[i][j].total_invoice,
                        total_outstanding : totalBilling[i][j].total_outstanding,
                    }
                }
                
            }
    
            // let data1 = foundCorpAccount[0].account_name;
            // let data2 = totalBilling[0][0].account_id
            // let data3 = foundCorpAccount[0].account_id
            // console.log('==========================')
            // console.log('==========================')
            // console.log(`account name : ${data1}`)
            // console.log(`totalBilling length: ${data2}`)
            // console.log(`foundCorpAccount length: ${data3}`)
    
            res.status(200).json({
                status : 'true',
                message : 'Billing of Corporate Account retrieved ',
                data : dataOutput
            })   
        }
    
        else {
            res.status(422).json({
                status: false,
                message: 'User not authorized',
                data: null,
            })
        }
    
    
        } 
        catch (err){
            res.status(500).json({
                status: false,
                message: err.message,
                data: null,
            })
        }
        
    
    }
    
    
    async unpaidBillingList (req, res) {
    
        try{
            const userActive = req.email;
        
            const foundPicCorpAcc = await pic_corporate_account.findOne({
                where:{
                    email: userActive.email
                }
            })
            console.log('============')
            console.log(foundPicCorpAcc.email)
            
            if (foundPicCorpAcc !== null){
                const foundCorpAccount = await corporate_account.findOne({  
                    where :{
                        pic_id:foundPicCorpAcc.pic_id
                    },
                    raw: true,
                    nest: true,
                })
            if(foundCorpAccount.length !== 0) {
                const listUnpaidBills= await billing.findAll({
                    where :{
                        [sequelize.Op.and]: [
                            { account_id: foundCorpAccount.account_id },
                            { status_payment: 'UNPAID' }
                          ]
                        },                    
                });
                if (listUnpaidBills.length !== 0){
                    res.status(200).json({
                        status : 'true',
                        message : 'Unpaid Bills Corporate Account retrieved ',
                        data : listUnpaidBills
                    })
    
                } else {
                    res.status(200).json({
                        status : 'true',
                        message : 'No Unpaid Bills for your Corporate Account',
                        data : listUnpaidBills 
                    })
                }
                
            }
            
    
            } else {
                res.status(422).json({
                    status: false,
                    message: 'User not authorized',
                    data: null,
                })    
    
            }
        
        }catch (err){
            res.status(500).json({
                status: false,
                message: err.message,
                data: null,
            })
        }
    
    // res.send('unpaid bill')
    
    }


}



module.exports = CorporateBillingController


