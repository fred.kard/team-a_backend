const jwt = require('jsonwebtoken');
const { pic_corporate_account } = require('../../../models')

const {
    hashSync,
    genSaltSync,
    compareSync
} = require("bcrypt");

class UserController {
    async editProfileCorporate(req, res, next) {
        
        const { name, password, nik, email, phone, job_position, workplace, avatar } = req.body
        console.log(req.body);
        
        if (req.businessLogic != undefined) {
            next()
            return
        }
        if (name == undefined || password == undefined  || nik == undefined  || email == undefined  || phone == undefined || job_position == undefined || workplace == undefined
            || name == "" || password == "" || nik == "" || email == "" || phone == "" || job_position == "" || workplace == "" ) {
                console.log('state gagal')
                res.status(201).json({
                    "status": false,
                    "message": "Column Not Complete",
                    "data": null
            })
            next()
            return
        }
        
        await pic_corporate_account.findAll({ where: { email:email} })
            .then(async data => {
                
                const salt = genSaltSync(10);
                const hashedPassword = hashSync(req.body.password, salt);

                if (data.length > 0) {
                    await pic_corporate_account.update({
                        name: name,
                        password: hashedPassword,
                        nik:nik,
                        phone:phone,
                        job_position:job_position,
                        workplace:workplace,
                        avatar: avatar,
                    }, {
                            where: {
                                email:email
                            }
                        }).then(
                            async data => {
                                res.status(201).json({
                                    "status": true,
                                    "message": "Update Profile Success",
                                    "data": { 
                                            "name": name,
                                            "password": hashedPassword,
                                            "nik": nik,
                                            "email": email,
                                            "phone": phone,
                                            "job_position": job_position,
                                            "workplace": workplace,
                                            "avatar": avatar,
                                     }
                                })
                                next()
                                return
                            }
                        ).catch(
                            async err => {
                                console.log(err)
                                res.status(401).json({
                                    "status": false,
                                    "message": "Update Profile Error",
                                    "data": null
                                })
                                next()
                                return
                            }
                        )
                } else {
                    res.status(401).json({
                        "status": false,
                        "message": "Update Profile error",
                        "data": null
                    })
                    next()
                    return
                }
            })
            .catch(async err => {
                console.log(err)
                res.status(500).json({
                    "status": false,
                    "message": err,
                    "data": null
                })
                next()
                return
            })
    }

}

module.exports = UserController
