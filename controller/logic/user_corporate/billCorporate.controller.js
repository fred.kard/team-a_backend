const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const billings = require('../../../models').billing;

class billCorporate{
    //feature untuk menampilkan profile tagihan secara lengkap
    async searchBill(req,res,next) {
        try{
            //mengambil data dari req.body
            const {account_id} = req.body
            console.log(req.body)

            //mengambil database dari table billings
            await billings.findAll({
                where: {
                    account_id : account_id
                }
            })
            .then(async data => {
                var listBill = []
                var index = data.length - 1

                //proses looping for untuk menampilkan data
                for (index; index >= 0;  --index){
                    listBill[index] = {
                        "billing_id" : data[index].dataValues.billing_id,
                        "account_id" : data[index].dataValues.account_id,
                        "month" : data[index].dataValues.month,
                        "year" : data[index].dataValues.year,
                        "total_invoice" : data[index].dataValues.total_invoice,
                        "total_outstanding" : data[index].dataValues.total_outstanding,
                        "virtual_account" : data[index].dataValues.virtual_account,
                        "status_payment" : data[index].dataValues.status_payment

                    }
                }
                res.status(200).json({
                    status: true,
                    message: "Attempt Success",
                    data: listBill
                })
                console.log(data)
            })
        }
        catch(error) {
            console.log(error)
            const bodyRes = {
                status: false,
                message: "Failed to submit",
                data: null
            }
            res.send(bodyRes)
            return
        }
    }
}


module.exports = billCorporate