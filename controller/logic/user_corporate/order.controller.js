const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const { order } = require('../../../models');
const { order_item } = require('../../../models');
const db = require('../../../models/index');
const { QueryTypes } = require('sequelize');
const Mailer = require('../mailer/sendMail');

class orderController{
    async getOrderForPIC(req, res, next) {
        try{
            const user_pic = req.email
            const pic = await pic_corporate_account.findOne({
                where:{
                    email: user_pic.email
                }
            })
            if (pic) {
                try {
                    const queryGetOrder = ("select corporate_accounts.account_name,pic_corporate_accounts.name, pic_corporate_accounts.email,pic_corporate_accounts.phone,orders.order_id,orders.am_id,orders.account_id,orders.pic_id,orders.order_type,orders.order_qty,orders.package_id_add_number, orders.status,\"orders\".\"createdAt\", \"orders\".\"updatedAt\" from orders left join corporate_accounts on orders.account_id = corporate_accounts.account_id left join pic_corporate_accounts on orders.pic_id = pic_corporate_accounts.pic_id left join order_items on orders.order_id = order_items.order_id where orders.pic_id = '"+ pic.pic_id +"' group by corporate_accounts.account_name,corporate_accounts.account_name,pic_corporate_accounts.name,pic_corporate_accounts.email,pic_corporate_accounts.phone,orders.order_id");
                    const data = await db.sequelize.query(queryGetOrder, { type: QueryTypes.SELECT });
                    if (data.length == 0 ) {
                        res.status(422).json({
                            status: false,
                            message: 'no data Order',
                            data: null,
                        })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: `List Order Retrieved!`,
                            data,
                        })
                    }
                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
            }
            else {
                res.status(422).json({
                    status: false,
                    message: 'Data not Found',
                    data: null,
                })
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }

    async getOrderItemsforPIC(req, res, next) {
        const postdata = req.params
        if (!postdata.order_id || postdata.order_id == "") {
            res.status(400).json({
                status: false,
                message: "Missing order_id Parameter!"
            })
            return
        }
        try {
            const data = await order_item.findAll({
                where:{
                    order_id: postdata.order_id
                }
            })
            if (data.length == 0) {
                res.status(422).json({
                    status: false,
                    message: 'no data Order Items',
                    data: null,
                })
            }
            else {
                res.status(200).json({
                    status: true,
                    message: `order Items retrieved!`,
                    data,
                })
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }
    
    async orderAddPackages(req, res, next) {
        const postdata = req.body
        if(postdata.numbers.length == 0 || !postdata.numbers) {
            res.status(400).json({
                status: false,
                message: "Missing list Numbers!"
            })
        }
        try{
            const user_corporate = req.email
            const pic = await pic_corporate_account.findOne({
                where:{
                    email: user_corporate.email
                }
            })
            const corporate = await corporate_account.findOne({
                where: {
                    pic_id: pic.pic_id,
                }
            })
            console.log(corporate.am_id)
            if (pic && corporate) {
                try {
                    const orders = await order.create({
                        order_type: "Add Packages",
                        am_id: corporate.am_id,
                        account_id: corporate.account_id,
                        pic_id: corporate.pic_id,
                        status: "open",
                        order_qty: postdata.numbers.length,
                    }); 
                    if (!orders) {
                        res.status(400).json({
                            status: false,
                            message: "failed create order",
                            orders
                        });
                        next()
                        return
                    }
                    let order_items = [];
                    try {
                        for(let index = postdata.numbers.length - 1; index >= 0; --index) {
                            order_items[index] = await order_item.create({
                                    order_id: orders.order_id,
                                    msisdn: postdata.numbers[index].msisdn,
                                    package_id: postdata.numbers[index].package_id,
                                    renewal: postdata.numbers[index].renewal,
                                    status: "open",
                                    order_type: "Add Packages",
                                }); 
                        }
                        res.status(200).json({
                            status: true,
                            message: "Your order has been submitted",
                            data: {
                                orders,
                                order_items,
                            }
                        });
                        next()
                        return
                    }
                    catch(err) {
                        res.status(500).json({
                            status: false,
                            message: err.message
                        })
                    }
                    next()
                    return
                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
                next()
                return
            }
            else {
                res.status(400).json({
                    status: false,
                    message: "data reference not found",
                    data: null
                });
                next()
                return
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }

    async getMyOrder(req, res, next) {
        try{
            const user_corporate = req.email
            const pic = await pic_corporate_account.findOne({
                where:{
                    email: user_corporate.email
                }
            })
            if (pic) {
                try {
                    const queryGetOrderItems = ("select order_items.* from order_items left join orders on order_items.order_id = orders.order_id where orders.pic_id = '"+ pic.pic_id +"';");
                    const data = await db.sequelize.query(queryGetOrderItems, { type: QueryTypes.SELECT });
                    if (data.length == 0) {
                        res.status(422).json({
                            status: false,
                            message: 'no Order Items',
                            data: null,
                        })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: `order Items retrieved!`,
                            data
                        })
                    }
                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }

    async orderAddNumbers(req, res, next) {
        try{
            const userActive = req.email;
            const orderQtty = req.body.quantity;
            const packageIdAddNumber = req.body.package_id;
        
            const foundPicCorpAcc = await pic_corporate_account.findOne({
                where:{
                    email: userActive.email
                }
            })
            
            //check data sent by user 
            if ( orderQtty.length !== 0 && packageIdAddNumber.length !== 0) {

                if (foundPicCorpAcc !== null){
                    // find account_id & am_id
                    const foundCorpAccount = await corporate_account.findOne({  
                        where :{
                            pic_id:foundPicCorpAcc.pic_id
                        },
                        raw: true,
                        nest: true,
                    })
                    
                    const orders = await order.create({
                                order_type: "Add Numbers",
                                am_id: foundCorpAccount.am_id,
                                account_id: foundCorpAccount.account_id,
                                pic_id: foundPicCorpAcc.pic_id,
                                status: "open",
                                order_qty: orderQtty,
                                package_id_add_number : packageIdAddNumber
                            }); 
                    
                     // send email notification 
                     let pathFile = './views/emailNotif.ejs'
                     let picName = foundPicCorpAcc.name
                     let emailRecipient = foundPicCorpAcc.email
                     let orderId = orders.order_id
                     let messageInfo = 'Quantity Additional MSISDN'         
                     const emailNotif = new Mailer(pathFile,emailRecipient,'Order Notification', 
                     {
                         orderId, 
                         picName, 
                         orderQtty,
                         messageInfo,
                         packageIdAddNumber 
                     })
                     emailNotif.send()
                    
                    //for testing only, bypass db create
                    // res.status(200).json({
                    //     status : true,
                    //     message : 'data order add number',
                    //     data : {
                    //         am_id: foundCorpAccount.am_id,
                    //         pic_id: foundPicCorpAcc.pic_id,
                    //         account_name: foundCorpAccount.account_name,
                    //         account_id: foundCorpAccount.account_id,
                    //         order_qty: orderQtty,
                    //         package_id_add_number : packageIdAddNumber
                    //     }
                    // })
    
                    res.status(200).json({
                        status : true,
                        message : 'success create order add number',
                        data : {
                            orders
                        }
                    })
                         
                } else {
                    res.status(422).json({
                        status : false,
                        message : 'Account not registered in the database',
                        data : null
                    })
                }
            } else {
                res.status(422).json({
                    status : false,
                    message : 'incomplete data',
                    data : null
                })
            }
           
        } catch (err) {
            res.status(500).json({
                status: false,
                message: err.message,
                data: null,
            })
        }
    }



}

module.exports = orderController