const jwt = require('jsonwebtoken');
const packages = require('../../../models').packages;
const { msisdn, corporate_account, account_manager, pic_corporate_account, msisdn_package, order_item, order } = require('../../../models');
const { Op } = require('sequelize');
const {
    hashSync,
    genSaltSync,
    compareSync
} = require("bcrypt");
const Mailer = require('../mailer/sendMail');

class packageController{
    async listPackage(req,res,next) {
        try{
            const { email, password, product_status } = req.body
            console.log(email)
            await packages.findAll({
                where : {
                    product_status : product_status,
                }
            })
            .then(async data => {
                var listPackages = []
                    var index = data.length - 1
                    for (index; index >= 0 ; --index){
                        listPackages[index] = {
                            "package_id": data[index].dataValues.package_id,
                            "package_type": data[index].dataValues.package_type,
                            "package_name": data[index].dataValues.package_name,
                            "package_value": data[index].dataValues.package_value,
                            "price": data[index].dataValues.price,
                            "product_status": data[index].dataValues.product_status
                        }
                    }
                res.status(200).json({
                    status: true,
                    message: "Attempt Authentication Success",
                    data: listPackages
                })
                console.log(data)
                }         
            )}
                   catch(error){
                    console.log(error)
                    const bodyRes = {
                        status: false,
                        message: "Failed to submit",
                        data: null
                }
                res.send(bodyRes)
                return
            }
        
        }
    async changePackagePic(req, res, next) {
        try{
            var requestbody = req.body
            var listNumberChange = []
            const userActive = req.email;
            const idPic = await pic_corporate_account.findOne({
                where: {
                    email: userActive.email
                }
            })
            const userPic = await corporate_account.findOne({
                where: {
                    pic_id: idPic.pic_id
                }
            })

            //validasi msisdn dan package
            // 1. validasi msisdn
            var index = requestbody.length - 1
            for (index; index >= 0; --index) {
                const cekMsisdn = await msisdn.findOne({
                    where: {
                        msisdn: requestbody[index].msisdn
                    }
                })
                if (!cekMsisdn) {
                    res.status(422).json({
                        status: false,
                        message: `MSISDN ${requestbody[index].msisdn} not belong to account_id`,
                        data: null,
                    })
                    return
                }
                var jumlah_paket_remove = requestbody[index].package_remove.length - 1
                var jumlah_paket_add = requestbody[index].package_new.length - 1

                //2. validasi paket sebelum di remove
                for (jumlah_paket_remove; jumlah_paket_remove >= 0; --jumlah_paket_remove) {
                    const cekPaketRemove = await msisdn_package.findOne({
                        where: {
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index].package_remove[jumlah_paket_remove].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index].msisdn } }
                            ]
                        }
                    })

                    //kondisi paket tidak bisa remove jika sebelumnya memang tidak ada
                    if (!cekPaketRemove) {
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_remove[jumlah_paket_remove].package_id} at MSISDN ${requestbody[index].msisdn} not listed, cannot removed`,
                            data: null,
                        })
                        return
                    }
                    const cekPaketRemoveNotActive = await msisdn_package.findOne({
                        where: {
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index].package_remove[jumlah_paket_remove].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index].msisdn } },
                                { is_active: { [Op.eq]: false } }
                            ]
                        }
                    })

                    //kondisi paket tidak bisa di remove jika sebelumnya telah di remove, ditandai dengan is_active = false
                    if (cekPaketRemoveNotActive) {
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_remove[jumlah_paket_remove].package_id} at MSISDN ${requestbody[index].msisdn} already removed, cannot removed again`,
                            data: null,
                        })
                        return
                    }

                    const cekOrderItemRemove = await order_item.findOne({
                        where: {
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index].package_remove[jumlah_paket_remove].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index].msisdn } },
                                { status: { [Op.eq]: "open" } }
                            ]
                        }
                    })

                    //kondisi paket tidak bisa di remove karena sedang proses untuk remove, wait approval AM
                    if (cekOrderItemRemove){
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_remove[jumlah_paket_remove].package_id} at MSISDN ${requestbody[index].msisdn} is on process remove, contact your AM`,
                            data: null,
                        })
                        return
                    }
                }

                //3. validasi paket sebelum di add
                for (jumlah_paket_add; jumlah_paket_add >= 0; --jumlah_paket_add) {
                    const cekPaketAdd = await msisdn_package.findOne({
                        where: {
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index].package_new[jumlah_paket_add].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index].msisdn } },
                                { is_active: { [Op.eq]: true } }
                            ]
                        }
                    })

                    //kondisi paket masih aktif sehingga tidak bisa ditambah ulang
                    if (cekPaketAdd) {
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_new[jumlah_paket_add].package_id} at MSISDN ${requestbody[index].msisdn} already listed, cannot duplicated`,
                            data: null,
                        })
                        return
                    }

                    const cekOrderItemAdd = await order_item.findOne({
                        where:{
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index].package_new[jumlah_paket_add].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index].msisdn } },
                                { status: { [Op.eq]: "open" } }
                            ]
                        }
                    })
                    if (cekOrderItemAdd) {
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_new[jumlah_paket_add].package_id} at MSISDN ${requestbody[index].msisdn} is on process add, contact your AM`,
                            data: null,
                        })
                        return
                    }
                }
            }

            var create_order = await order.create({
                am_id: userPic.am_id,
                account_id: userPic.pic_id, // disini kalo mau pake req body :requestbody[0].account_id,
                pic_id: userPic.pic_id,
                status: "open",
                order_type: "Change Packages",
                order_qty: requestbody.length
            })
            var index2 = requestbody.length - 1

            //looping untuk mengetahui jumlah msisdn yang berubah paket
            for (index2; index2 >= 0; --index2) {
                var jumlah_paket_remove2 = requestbody[index2].package_remove.length - 1
                var jumlah_paket_add2 = requestbody[index2].package_new.length - 1
                var listPackage = []

                //upate db order item untuk paket remove
                for (jumlah_paket_remove2; jumlah_paket_remove2 >= 0; --jumlah_paket_remove2) {
                    await order_item.create({
                        order_id: create_order.order_id,
                        msisdn: requestbody[index2].msisdn,
                        package_id: requestbody[index2].package_remove[jumlah_paket_remove2].package_id,
                        renewal: false,
                        status: "open",
                        order_type: "Remove"
                    }).then(async data => {
                        listPackage.push({
                            'package_id': data.dataValues.package_id,
                            'order_type': "Remove",
                        })
                    })
                }

                //update db order item untuk paket add
                for (jumlah_paket_add2; jumlah_paket_add2 >= 0; --jumlah_paket_add2) {
                    await order_item.create({
                        order_id: create_order.order_id,
                        msisdn: requestbody[index2].msisdn,
                        package_id: requestbody[index2].package_new[jumlah_paket_add2].package_id,
                        renewal: true,
                        status: "open",
                        order_type: "New"
                    }).then(async data => {
                        listPackage.push({
                            'package_id': data.dataValues.package_id,
                            'order_type': "New",
                        })
                    })

                    //jika sebelumnya pernah beli paket, maka cukup di update active renewal,start_date, expire_date
                    const cekPaketAddRenewal = await msisdn_package.findOne({
                        where: {
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index2].package_new[jumlah_paket_add2].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index2].msisdn } }
                            ]
                        }
                    })
                }
                listNumberChange.push({
                    "msisdn": requestbody[index2].msisdn,
                    "package": listPackage
                })
            }

            // send email notification order change package
            let pathFile = './views/emailNotif.ejs'
            let picName = idPic.name //
            let emailRecipient = idPic.email
            let orderId = create_order.order_id
            let orderQtty = create_order.order_qty
            let messageInfo = 'Change Packages'         
            const emailNotif = new Mailer(pathFile,emailRecipient,'Order Notification', 
            {
                orderId, 
                picName, 
                orderQtty,
                messageInfo
            })
            emailNotif.send()

            res.status(201).json({
                status: true,
                message: "Your changes has been saved!",
                data: listNumberChange
            })

        } catch(error){
            console.log(error)
            res.status(500).json({
                status: false,
                message: error.message,
                data: null
            })
        }
    }
}
    
 module.exports = packageController
                
                
        