const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const { table_ticket } = require('../../../models');


class ticketController{
    // feature untuk membuat ticket by PIC Corporate
    async createTicket(req, res, next) {
        try{
            var user_corporate = req.email
            const id_pic = await pic_corporate_account.findAll({
                where:{
                    email: user_corporate.email
                }
            })
            const postdata = req.body

            //pengecekan field data
            if(!postdata.ticket_category || !postdata.headline)
            throw{
                status: false,
                message: "Failed to submit, check your field",
                data: null
            }

            const id_am = await corporate_account.findAll({
                where: {
                    pic_id: id_pic[0].dataValues.pic_id
                }
            })

            await table_ticket.create({
                ticket_category: postdata.ticket_category,
                am_id: id_am[0].dataValues.am_id,
                account_id: id_am[0].dataValues.account_id,
                pic_id: id_pic[0].dataValues.pic_id,
                status: "open",
                comment: postdata.headline
            }).then(async data => {
                res.status(201).json({
                    "status": true,
                    "message": "Your ticket has been submitted",
                    "data": {
                        "ticket_category": data.ticket_category,
                        "am_id": data.am_id,
                        "account_id": data.account_id,
                        "account_name":id_am[0].dataValues.account_name,
                        "pic_id": data.pic_id,
                        "status": "open",
                        "headline": data.comment,
                        "createdAt": data.createdAt,
                        "ticket_id": data.ticket_id
                    }
                });
                next()
                return
            }).catch((err) => {
                throw new Error(err.toString());
            })
        }
    catch(error) {
        console.log(error)
            res.status(201).json({
                status: false,
                message: error.message,
                data: null
            })
        return
        }
    }

    // feature untuk mengetahui ticket yang sudah di submit PIC Corporate
    async viewUserTicket(req, res, next){
        try{
            var user_corporate = req.email
            const id_pic = await pic_corporate_account.findAll({
                where: {
                    email: user_corporate.email
                }
            })

            await table_ticket.findAll({
                where:{
                    pic_id: id_pic[0].dataValues.pic_id
                }
            }).then(async data =>{
                
                // pengecekan jika tidak mempunyai ticket
                if (data.length == 0) {
                    res.status(200).json({
                        status: false,
                        message: "you dont have any Ticket",
                        data: null,
                    })
                } else {
                    
                    // jika mempunyai ticket maka akan di tampilkan
                    var listticket = []
                    var index = data.length - 1
                    for (index; index >= 0; --index) {
                        const name = await corporate_account.findAll({
                            where: {
                                account_id: data[index].account_id
                            }
                        })

                        listticket[index] = {
                            ticket_id: data[index].ticket_id,
                            ticket_category: data[index].ticket_category,
                            am_id: data[index].am_id,
                            account_id: data[index].account_id,
                            account_name: name[0].account_name,
                            pic_id: data[index].pic_id,
                            status: data[index].status,
                            headline: data[index].comment,
                            action: data[index].action,
                            createdAt: data[index].createdAt,
                            updatedAt: data[index].updatedAt,
                        }
                    }
                    res.status(200).json({
                        status: true,
                        message: "List Ticket",
                        data: listticket,
                    })
                    next()
                    return
                }

            })
        }
        catch(error){
            console.log(error)
            res.status(500).json({
                status: false,
                message: error.message,
                data: null
            })
            return  
        }
    }
    
    // Feature digunakan untuk closing ticket by PIC Corporate
    async closeTicket(req, res, next) {
        
        // Memberikan inputan ticket_id pada body
        const postdata = req.body
        if(!postdata.ticket_id || !postdata.action) {
            res.status(200).json({
                status: false,
                message: "Missing ticket id!"
            })
        }

        try{
            // Pengecekan user PIC Corporate
            const user_corporate = req.email
            const pic = await pic_corporate_account.findOne({
                where:{
                    email: user_corporate.email
                }
            })

            // Pengecekan Corporate Account
            const corporate = await corporate_account.findOne({
                where: {
                    pic_id: pic.pic_id,
                }
            })
            
            // Perubahan status ticket
            if (pic && corporate) {
                try { 
                    
                    const updateTicket = await table_ticket.update({
                        
                        status: "close",
                        action: postdata.action
                        
                    }, {where: {

                        account_id: corporate.account_id,
                        ticket_id: postdata.ticket_id
                    }}
                    )
                    try{
                        if  (updateTicket == 1) {
                            const data = await table_ticket.findOne({
                                where:{
                                   ticket_id: postdata.ticket_id
                                }
                            })
                            res.status(200).json({
                                status: true,
                                message: 'Your Ticket has been close',
                                data
                            })
                        }
                        else {
                            
                            res.status(201).json({
                                status: false,
                                message: 'close ticket failed',
                                data: null,
                            })
                        }
                    }
                    catch(err) {
                        res.status(500).json({
                            status: false,
                            message: err.message
                        })
                    }                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
            }
            else {

            }
        }
        catch(err) {
            res.status(401).json({
                status: false,
                message: err.message
            })
        }
    }
}

module.exports = ticketController