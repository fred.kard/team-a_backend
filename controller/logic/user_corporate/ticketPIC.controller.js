const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const { table_ticket } = require('../../../models');
const { ticket_detail } = require('../../../models');
const { account_manager } = require('../../../models');


class ticketPICcontroller{
    
    async myTicketDetailPIC(req, res, next) {
        try{
            
            // Memberikan inputan params dengan ticket_id
            const id_ticket = req.query

            console.log(id_ticket)
            if (!id_ticket.ticket_id){
                res.status(201).json({
                    status: false,
                    message: 'Please insert id ticket first',
                    data: null,
                })
            }

            // Untuk query data pada tabel table_ticket
            const a = await table_ticket.findAll({
                where: { 
                    ticket_id: id_ticket.ticket_id
                }
            })
                        
           // Mengambil data user PIC
            const id_pic = await pic_corporate_account.findAll({
                where: {
                    pic_id: a[0].dataValues.pic_id
                }
            })

            // Mengambil data corporate
            const id_corporate = await corporate_account.findAll({
                where: {
                    account_id: a[0].dataValues.account_id
                }
            })

            // Untuk query total data pada tabel ticket_detail
            await ticket_detail.findAll({
                where: {
                    ticket_id: a[0].dataValues.ticket_id
                }
            })

             .then(async data => {
                
                console.log(data)
                var reply = []
                var index = data.length - 1 
                console.log(index)
                
                // Untuk mengambil seluruh data pada tabel ticket detail sesuai dengan ticket_id
                for (index; index >= 0; --index) {
                  const nama = await ticket_detail.findAll({
                        where: {
                            ticket_id: a[0].dataValues.ticket_id
                        }
                    })
                        reply[index] = {
                            name: nama[index].name,
                            office_name: nama[index].office_name,
                            comments: nama[index].comments,
                            createdAt: nama[index].createdAt,
                        }
                    }

                res.status(200).json({
                "status": true,
                "message": "Your ticket detail",
                "data": {
                    "ticket_id": a[0].dataValues.ticket_id,
                    "ticket_category": a[0].dataValues.ticket_category,
                    "am_id": a[0].dataValues.am_id,
                    "name": id_pic[0].dataValues.name,
                    "account_name": id_corporate[0].dataValues.account_name,
                    "pic_id": a[0].dataValues.pic_id,
                    "status": a[0].dataValues.status,
                    "account_id": a[0].dataValues.account_id,
                    "comment": a[0].dataValues.comment,
                    "created_at": a[0].dataValues.createdAt,
                    "history_reply" : reply,
                }
            });
            next()
            return
        }).catch((err) => {
            throw new Error(err.toString());
        })
    } catch(error){
        console.log(error)
        const bodyRes = {
            status: false,
            message: "Failed to get detail ticket",
            data: null
        }
        res.send(bodyRes)
        return
        }
    }

    async replyComplainPIC(req, res, next){
        try{

            // Mengambil data user AM
            var user_pic = req.email
            const id_user = await pic_corporate_account.findAll({
                where: { 
                    email: user_pic.email
                }
            })

            // Memberikan inputan request pada body
            const postdata = req.body
            if(!postdata.ticket_id || !postdata.comments )
            throw {
                status: false,
                message: "Failed to submit, check your field",
                data: null
            }

            // pengecekan ticket status open/close
            const cekStatus = await table_ticket.findAll({
                where: {
                        ticket_id: postdata.ticket_id
                }    
            })   

            // Menyimpan ke database pada tabel ticket detail
            await ticket_detail.create({
                ticket_id: postdata.ticket_id,
                comments: postdata.comments,
                name: id_user[0].dataValues.name,
                office_name: "Corporate",

            }).then(async data => {

                var status = cekStatus[0].dataValues.status;
                if (postdata.ticket_id && status == 'open'){

                // Update tanggal di table_ticket untuk counting feature auto close ticket
                await table_ticket.update({

                    response: "reply comment"                
                    
                }, {where: {
                    ticket_id: postdata.ticket_id
                }}
                )

                res.status(201).json({
                    "status": true,
                    "message": "Your reply has been sent",
                    "data": {
                        "ticket_id": postdata.ticket_id,
                        "name": id_user[0].dataValues.name,
                        "office_name": data.office_name,
                        "comments": postdata.comments,
                        "reply_at": data.createdAt,
                    }
                })
            } else {
                res.status(201).json({
                    "status": true,
                    "message": "Your ticket has been close, you can't post reply",
                    "data": null,

                })
            }            

                next()
                return
            }).catch((err) => {
                throw new Error(err.toString());
            })
        } catch(error){
            console.log(error)
            const bodyRes = {
                status: false,
                message: "Failed to submit reply",
                data: null
            }
            res.send(bodyRes)
            return
        }
    }
}

module.exports = ticketPICcontroller