const { account_manager } = require('../../../models');
const { order } = require('../../../models');
const { order_item } = require('../../../models');
const { msisdn } = require('../../../models');
const { msisdn_package } = require('../../../models');
const db = require('../../../models/index');
const {Op} = require('sequelize');
const { QueryTypes } = require('sequelize');
const { pic_corporate_account } = require('../../../models');
const Mailer = require('../mailer/sendMail');


class orderController{
    //end point not used
    async getMyOrderItemsByAMID(req, res, next) {
        try{
            const user_am = req.email
            const am = await account_manager.findOne({
                where:{
                    email: user_am.email
                }
            })
            
            if (am) {
                console.log("test"+ am.am_id)
                try {
                    const queryGetOrderItems = ("select corporate_accounts.account_name,pic_corporate_accounts.name, pic_corporate_accounts.email, pic_corporate_accounts.phone, order_items.* from orders left join corporate_accounts on orders.account_id = corporate_accounts.account_id left join pic_corporate_accounts on orders.pic_id = pic_corporate_accounts.pic_id left join order_items on orders.order_id = order_items.order_id where orders.am_id = '"+ am.am_id +"';");
                    const data = await db.sequelize.query(queryGetOrderItems, { type: QueryTypes.SELECT });
                    if (data.length == 0) {
                        res.status(422).json({
                            status: false,
                            message: 'no Order Items',
                            data: null,
                        })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: `order Items retrieved!`,
                            data
                        })
                    }
                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
            }
            else {
                res.status(422).json({
                    status: false,
                    message: 'Data not Found',
                    data: null,
                })
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }

    async getOrderForAM(req, res, next) {
        try{
            const user_am = req.email
            const am = await account_manager.findOne({
                where:{
                    email: user_am.email
                }
            })
            if (am) {
                try {
                    const queryGetOrder = ("select corporate_accounts.account_name,pic_corporate_accounts.name, pic_corporate_accounts.email,pic_corporate_accounts.phone,orders.order_id,orders.am_id,orders.account_id,orders.pic_id,orders.order_type,orders.order_qty,orders.package_id_add_number,orders.status, \"orders\".\"createdAt\", \"orders\".\"updatedAt\" from orders left join corporate_accounts on orders.account_id = corporate_accounts.account_id left join pic_corporate_accounts on orders.pic_id = pic_corporate_accounts.pic_id left join order_items on orders.order_id = order_items.order_id where orders.am_id = '"+ am.am_id +"' group by corporate_accounts.account_name,corporate_accounts.account_name,pic_corporate_accounts.name,pic_corporate_accounts.email,pic_corporate_accounts.phone,orders.order_id");
                    const data = await db.sequelize.query(queryGetOrder, { type: QueryTypes.SELECT });
                    if (data.length == 0) {
                        res.status(422).json({
                            status: false,
                            message: 'no data Order',
                            data: null,
                        })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: `List Order Retrieved!`,
                            data,
                        })
                    }
                }
                catch(err) {
                    res.status(500).json({
                        status: false,
                        message: err.message
                    })
                }
            }
            else {
                res.status(422).json({
                    status: false,
                    message: 'Data not Found',
                    data: null,
                })
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }

    async getOrderItemsforAM(req, res, next) {
        const postdata = req.params
        if (!postdata.order_id || postdata.order_id == "") {
            res.status(400).json({
                status: false,
                message: "Missing order_id Parameter!"
            })
            return
        }
        try {
            const data = await order_item.findAll({
                where:{
                    order_id: postdata.order_id
                }
            })
            if (data.length == 0) {
                res.status(422).json({
                    status: false,
                    message: 'no data Order Items',
                    data: null,
                })
            }
            else {
                res.status(200).json({
                    status: true,
                    message: `order Items retrieved!`,
                    data,
                })
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }

    async changeStatusOrder(req, res, next) {
        try {
            const userActive = req.email
            const userActiveEmail = userActive.email
            
            const dataUserActiveRaw = await account_manager.findOne({where: {email: userActiveEmail}})
            const dataUserActive = dataUserActiveRaw.dataValues
            
            const reqOrderId = req.body.order_id;
            const reqStatus = req.body.status
            //error handling input not proper
            if(
                reqOrderId === undefined || reqOrderId == "" || isNaN(reqOrderId) ||
                reqStatus === undefined || reqStatus == ""
            ) {
                res.status(422).json({
                    status: false,
                    message: `Falied, check your input`,
                    data: null
                })
            }
            
            const dataOrderRaw = await order.findOne({where: {order_id: reqOrderId}})
            //error handling order id not found
            if(!dataOrderRaw) {
                res.status(422).json({
                    status: false,
                    message: `Falied, order id ${reqOrderId} not found`,
                    data: null,
                })
            }
            const dataOrder = dataOrderRaw.dataValues

            //error handling order not belong to user
            if(dataOrder.am_id != dataUserActive.am_id) {
                res.status(422).json({
                    status: false,
                    message: `Falied, order id ${reqOrderId} not belong to you`,
                    data: null,
                })
            }

            //change open order to completed || canceled
            if(dataOrder.status == "open") {
                if(reqStatus == "canceled") {
                    await order.update(
                        {status: "canceled"},
                        {where: {order_id: {[Op.eq]: reqOrderId} } }
                    )
                    await order_item.update(
                        {status: "canceled"},
                        {where: {order_id: {[Op.eq]: reqOrderId} } }
                    )
                    let updatedOrderCancel = await order.findOne({where: {order_id: reqOrderId}})
                    
                    //send email notification
                    const pic = await pic_corporate_account.findOne({
                        where:{
                            pic_id: updatedOrderCancel.pic_id
                        }
                    })
                    
                    let pathFile = './views/emailNotif.ejs'
                    let picName = pic.name
                    let emailRecipient = pic.email
                    let orderId = updatedOrderCancel.order_id
                    let messageInfo = 'Order Cancel'
                                                    
                    const emailNotif = new Mailer(pathFile,emailRecipient,'Order Cancelation', 
                            {
                                orderId, 
                                picName, 
                                messageInfo 
                            })
                            emailNotif.send()



                    res.status(200).json({
                        status: true,
                        message: `Success, order id ${updatedOrderCancel.order_id} canceled`,
                        data: updatedOrderCancel,
                    })
                }
                else if (reqStatus == "completed") {
                    //executing "Add Numbers"
                    if(dataOrder.order_type == "Add Numbers") {
                        //check exsisting msisdn
                        let exsistingMsisdnArray = []
                        const existingMsisdnRaw = await msisdn.findAll({where: {account_id: dataOrder.account_id} })
                        for (let i = 0; i < existingMsisdnRaw.length; i++) {
                            exsistingMsisdnArray.push(existingMsisdnRaw[i].dataValues.msisdn)
                        }
                        //generate data msisdn & makes sure theres no duplicate msisdn 
                        let newDataMsisdnArray = []
                        let newDataMsisdn ;
                        let newMsisdnArray = []
                        let newMsisdn;
                        function generateDataMsisdn() {
                            newMsisdn = '628110' + dataOrder.account_id + Math.floor(1000 + Math.random() * 9000)
                            if(checkExsisting(newMsisdn) && checkGeneratedMsisdn(newMsisdn)) {
                                newMsisdnArray.push(newMsisdn);
                                newDataMsisdn = {
                                    msisdn: newMsisdn,
                                    full_name   : 'Karyawan perusahaan id ' + dataOrder.account_id,
                                    imsi        : '5101054110' + dataOrder.account_id + newMsisdn[8] + newMsisdn[9] + newMsisdn[10],
                                    iccid       : '985467283418764' + Math.floor(1000 + Math.random() * 9000),
                                    puk_1       : '35958' + Math.floor( Math.random()*999 ),
                                    puk_2       : '95802' + Math.floor( Math.random()*999 ),
                                    account_id  : +dataOrder.account_id,
                                    is_active   : true
                                }
                                newDataMsisdnArray.push(newDataMsisdn)
                            return newMsisdn
                            }
                        
                            return generateDataMsisdn()
                        }
                    
                        function checkGeneratedMsisdn(el) {
                            for( let i = 0; i < newMsisdnArray.length; i++) 
                                if(newMsisdnArray[i] == el) return false;
                            return true;
                        }
                    
                        function checkExsisting(el) {
                            for (let i = 0; i < exsistingMsisdnArray.length; i++) 
                                if(exsistingMsisdnArray[i] == el) return false;
                            return true;
                        }
                    
                        for( let i = 0; i < dataOrder.order_qty; i++ ) {
                            generateDataMsisdn()
                        }
                    
                        //create msisdn & msisdn_package & update order
                        let listMsidnCreated = []
                        for (let i = 0; i < newDataMsisdnArray.length; i++ ) {
                            var addManyMsisdn = await msisdn.create({
                                msisdn      : newDataMsisdnArray[i].msisdn,
                                full_name   : newDataMsisdnArray[i].full_name,
                                imsi        : newDataMsisdnArray[i].imsi,
                                iccid       : newDataMsisdnArray[i].iccid,
                                puk_1       : newDataMsisdnArray[i].puk_1,
                                puk_2       : newDataMsisdnArray[i].puk_2,
                                account_id  : newDataMsisdnArray[i].account_id,
                                is_active   : newDataMsisdnArray[i].is_active,
                            })
                            var addMsisdnPackage = await msisdn_package.create({
                                msisdn      : newDataMsisdnArray[i].msisdn,
                                package_id  : dataOrder.package_id_add_number,
                                renewal     : true,
                                is_active   : true,
                                start_date  : Date.now(),
                                expire_date : '9999-01-01 07:00:00'
                            })
                            listMsidnCreated.push({
                                msisdn: newMsisdnArray[i],
                                package: dataOrder.package_id_add_number
                            })
                        }
                        await order.update(
                            {status: 'completed'},
                            {where: {order_id: dataOrder.order_id}}
                        )
                        let updatedOrderNewPackage = await order.findOne({where: {order_id: dataOrder.order_id}})
                        let resultNewPakcage = {
                            am_id                   : updatedOrderNewPackage.dataValues.am_id,
                            account_id              : updatedOrderNewPackage.dataValues.account_id,
                            pic_id                  : updatedOrderNewPackage.dataValues.pic_id,
                            status                  : updatedOrderNewPackage.dataValues.status,
                            order_type              : updatedOrderNewPackage.dataValues.order_type,
                            order_qty               : updatedOrderNewPackage.dataValues.order_qty,
                            package_id_add_number   : updatedOrderNewPackage.dataValues.package_id_add_number,
                            numbers                 : listMsidnCreated
                        }
                        if(addManyMsisdn && addMsisdnPackage && updatedOrderNewPackage) {
                            
                            //send email notification
                            const pic = await pic_corporate_account.findOne({
                                where:{
                                    pic_id: resultNewPakcage.pic_id
                                }
                            })

                            let pathFile = './views/emailNotif.ejs'
                            let picName = pic.name
                            let emailRecipient = pic.email
                            let orderId = dataOrder.order_id
                            let messageInfo = 'Order Completed'
                            let orderQtty = resultNewPakcage.order_qty
                            let packageIdAddNumber = addMsisdnPackage.package_id_add_number        
                            const emailNotif = new Mailer(pathFile,emailRecipient,messageInfo, 
                            {
                                orderId, 
                                picName, 
                                orderQtty,
                                messageInfo,
                                packageIdAddNumber,
                                listMsidnCreated 
                            })
                            emailNotif.send()
                            
                            res.status(200).json({
                                status: true,
                                message: `Success! order id ${updatedOrderNewPackage.dataValues.am_id} completed, add ${updatedOrderNewPackage.dataValues.order_qty} numbers at account id ${updatedOrderNewPackage.dataValues.account_id}`,
                                data: resultNewPakcage
                            })
                        }
                    }
                    
                    //executing "Change Packages"
                    if(dataOrder.order_type == "Change Packages") {
                        //get all "Change Packages" ("New" & "Remove")
                        const dataOrderItemRaw = await order_item.findAll({where: {order_id: dataOrder.order_id}})
                        const countOfOrderItem = dataOrderItemRaw.length
                        const dataOrderItem = []
                        const dataOrderRemovePackage = []
                        const dataOrderNewPackage = []
                        const listMsisdnDataOrder = []

                        for(let i = 0; i < countOfOrderItem; i++) {
                            dataOrderItem.push(dataOrderItemRaw[i].dataValues)
                            if (listMsisdnDataOrder.includes(dataOrderItemRaw[i].dataValues.msisdn) === false) {
                                listMsisdnDataOrder.push(dataOrderItemRaw[i].dataValues.msisdn)
                            }
                            if(dataOrderItemRaw[i].dataValues.order_type == "New") {
                                dataOrderNewPackage.push(dataOrderItemRaw[i].dataValues)
                            }
                            if(dataOrderItemRaw[i].dataValues.order_type == "Remove") {
                                dataOrderRemovePackage.push(dataOrderItemRaw[i].dataValues)
                            }
                        }

                        //validate packages to be deleted
                        for(let i = 0; i < dataOrderRemovePackage.length; i++) {
                            var checkRemovePackage = await msisdn_package.findOne({
                                where: {
                                    [Op.and]: [
                                        { msisdn: { [Op.eq]: dataOrderRemovePackage[i].msisdn }},
                                        { package_id: { [Op.eq]: dataOrderRemovePackage[i].package_id }},
                                        { is_active: { [Op.eq]: true }}
                                    ]
                                }
                            })
                            if(!checkRemovePackage) {
                                res.status(422).json({
                                    status: false,
                                    message: `Failed, can't remove package due to msisdn number ${dataOrderRemovePackage[i].msisdn} hasn't activated package id ${dataOrderRemovePackage[i].package_id} `,
                                    data: null,
                                })
                                return
                            }
                        }

                        //validate packages to be added
                        for(let i = 0; i < dataOrderNewPackage.length; i++) {
                            var checkNewPackage = await msisdn_package.findOne({
                                where: {
                                    [Op.and]: [
                                        { msisdn: { [Op.eq]: dataOrderNewPackage[i].msisdn }},
                                        { package_id: { [Op.eq]: dataOrderNewPackage[i].package_id }},
                                        { is_active: { [Op.eq]: true }}
                                    ]
                                }
                            })
                            if(checkNewPackage) {
                                res.status(422).json({
                                    status: false,
                                    message: `Failed, can't add package due to msisdn number ${dataOrderNewPackage[i].msisdn} already activated package id ${dataOrderNewPackage[i].package_id}`,
                                    data: null,
                                })
                                return
                            }
                        }

                        //re-arrange data remove package by msisdn to get proper output 
                        function reArangePackageRmvByMsisdn(msisdn) {
                            let packageRmvByMsisdn = []
                            for(let i = 0; i < dataOrderRemovePackage.length; i++) {
                                if(dataOrderRemovePackage[i].msisdn == msisdn) {
                                    packageRmvByMsisdn.push({'package_id': dataOrderRemovePackage[i].package_id})
                                }
                            }
                            return packageRmvByMsisdn
                        }

                        //re-arrange data add package by msisdn to get proper output 
                        function reArangePackageAddByMsisdn(msisdn) {
                            let packageAddByMsisdn = []
                            for(let i = 0; i < dataOrderNewPackage.length; i++) {
                                if(dataOrderNewPackage[i].msisdn == msisdn) {
                                    packageAddByMsisdn.push({'package_id': dataOrderNewPackage[i].package_id})
                                }
                            }
                            return packageAddByMsisdn
                        }
                    
                        //re-arange array by msisdn
                        const dataOrderItemShortByMsisdn = []
                        for(let i = 0; i < listMsisdnDataOrder.length; i++) {
                            dataOrderItemShortByMsisdn.push({
                                'msisdn': listMsisdnDataOrder[i],
                                'package_remove': reArangePackageRmvByMsisdn(listMsisdnDataOrder[i]),
                                'package_add': reArangePackageAddByMsisdn(listMsisdnDataOrder[i]),
                            })
                        }

                        // executing one by one each msisdn
                        for(let i = 0; i < dataOrderItemShortByMsisdn.length; i++) {
                            let countRmvPackageByMsisdn = dataOrderItemShortByMsisdn[i].package_remove.length
                            let countAddPackageByMsisdn = dataOrderItemShortByMsisdn[i].package_add.length

                            // remove package 
                            for(let j = 0; j < countRmvPackageByMsisdn; j++) {
                                await msisdn_package.update(
                                    {
                                        renewal: false,
                                        is_active: false,
                                        expire_date: Date.now()
                                    },
                                    {   
                                        where: {
                                            [Op.and]: [
                                                { package_id: { [Op.eq]: dataOrderItemShortByMsisdn[i].package_remove[j].package_id } },
                                                { msisdn: { [Op.eq]: dataOrderItemShortByMsisdn[i].msisdn } }
                                            ]
                                        }
                                    }
                                )
                                
                                await order_item.update(
                                    {status: "completed"},
                                    {
                                        where: {
                                            [Op.and]: [
                                                { package_id: { [Op.eq]: dataOrderItemShortByMsisdn[i].package_remove[j].package_id } },
                                                { msisdn: { [Op.eq]: dataOrderItemShortByMsisdn[i].msisdn } },
                                                { status: { [Op.eq]: "open"}}
                                            ]
                                        }
                                    }
                                )
                                
                            }

                            //add package
                            for(let k = 0; k < countAddPackageByMsisdn; k++) {
                                await msisdn_package.create(
                                    {
                                        msisdn: dataOrderItemShortByMsisdn[i].msisdn,
                                        package_id: dataOrderItemShortByMsisdn[i].package_add[k].package_id,
                                        renewal: true,
                                        is_active: true,
                                        start_date: Date.now(),
                                        expire_date: '9999-01-01 07:00:00'
                                    },
                                    {   
                                        where: {
                                            [Op.and]: [
                                                { package_id: { [Op.eq]: dataOrderItemShortByMsisdn[i].package_add[k].package_id } },
                                                { msisdn: { [Op.eq]: dataOrderItemShortByMsisdn[i].msisdn } }
                                            ]
                                        }
                                    }
                                )
                                
                                await order_item.update(
                                    {status: "completed"},
                                    {
                                        where: {
                                            [Op.and]: [
                                                { package_id: { [Op.eq]: dataOrderItemShortByMsisdn[i].package_add[k].package_id } },
                                                { msisdn: { [Op.eq]: dataOrderItemShortByMsisdn[i].msisdn } },
                                                { status: { [Op.eq]: "open"}}
                                            ]
                                        }
                                    }
                                )
                            }
                        }
                    
                        //completed order status
                        await order.update(
                            {status: 'completed'},
                            {where: {order_id: dataOrder.order_id}}
                        )
                        
                        let updatedOrderChangePackage = await order.findOne({where: {order_id: dataOrder.order_id}})
                        let resultChangePackage = {
                            am_id       : updatedOrderChangePackage.dataValues.am_id,
                            account_id  : updatedOrderChangePackage.dataValues.account_id,
                            pic_id      : updatedOrderChangePackage.dataValues.pic_id,
                            status      : updatedOrderChangePackage.dataValues.status,
                            order_type  : updatedOrderChangePackage.dataValues.order_type,
                            order_qty   : updatedOrderChangePackage.dataValues.order_qty,
                            numbers     : dataOrderItemShortByMsisdn
                        }
                        res.status(200).json({
                            status: true,
                            message: `Success! order id ${dataOrder.order_id} completed, change package of ${updatedOrderChangePackage.dataValues.order_qty} numbers at account id ${updatedOrderChangePackage.dataValues.account_id}`,
                            data: resultChangePackage
                        })

                    }
                }
                else {
                    res.status(422).json({
                        status: false,
                        message: `Failed, status ${reqStatus} unidentified, please choose completed or canceled `,
                        data: null,
                    })
                }
            } 
            else {
                res.status(422).json({
                    status: false,
                    message: `Falied, status order id ${reqOrderId} already ${dataOrder.status}`,
                    data: null,
                })
            }
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: error.message,
                data: error
            })
        }
    }

    async changeStatusOrderItem(req, res, next) {
        try {
            const userActive = req.email
            const userActiveEmail = userActive.email
            
            const dataUserActiveRaw = await account_manager.findOne({where: {email: userActiveEmail}})
            const dataUserActive = dataUserActiveRaw.dataValues

            const reqBody = req.body
            const qtyOfChange = req.body.length
            for(let i = 0; i < qtyOfChange; i++) {
                if(
                    reqBody[i].order_item_id == undefined || reqBody[i].order_item_id == "" || isNaN(reqBody[i].order_item_id) ||
                    reqBody[i].status == undefined || reqBody[i].status == ""
                ) {
                    res.status(422).json({
                        status: false,
                        message: `Falied, check your input`,
                        data: null
                    })
                }
                if(reqBody[i].status != "completed" && reqBody[i].status != "canceled") {
                    res.status(422).json({
                        status: false,
                        message: `Falied, you can only completed or canceled the status of order item `,
                        data: null
                    })
                    return
                }
            }

            let dataOrderItem = []
            let dataOrder = []
            for(let i = 0; i < qtyOfChange; i++) {    
                const dataOrderItemRaw = await order_item.findOne({where: {
                    [Op.and]: [
                        {id: {[Op.eq]: reqBody[i].order_item_id} },
                        {status: {[Op.eq]: "open"} }
                    ]}
                })
                //error handling order item id not found
                if(!dataOrderItemRaw) {
                    res.status(422).json({
                        status: false,
                        message: `Falied, order item id ${reqBody[i].order_item_id} not found or status not open`,
                        data: null
                    })
                    return
                }
                dataOrderItem.push(dataOrderItemRaw.dataValues)

                const dataOrderRaw = await order.findOne({where: {order_id: {[Op.eq]: dataOrderItem[i].order_id} } })
                //error handling order items not belong to user
                if(dataOrderRaw.dataValues.am_id != dataUserActive.am_id) {
                    res.status(422).json({
                        status: false,
                        message: `Falied, order Item id ${reqBody[i].order_item_id} not belong to you`,
                        data: null,
                    })
                    return
                }
                dataOrder.push(dataOrderRaw.dataValues)
            }
            //validate status
            var validOrderItemToCancel = []
            var validOrderItemToRmvAdd = []
            for(let i = 0; i < dataOrderItem.length; i++) {
                if(reqBody[i].status == "canceled") {
                    validOrderItemToCancel.push(dataOrderItem[i])
                }
                if(reqBody[i].status == "completed") {
                    let existancePackage = await msisdn_package.findOne({
                        where: {
                            [Op.and]: [
                                { msisdn: { [Op.eq]: dataOrderItem[i].msisdn }},
                                { package_id: { [Op.eq]: dataOrderItem[i].package_id }},
                                { is_active: { [Op.eq]: true }}
                            ]
                        }
                    })
                    //validate packages before remove
                    if(!existancePackage && dataOrderItem[i].order_type == "Remove") {
                        res.status(422).json({
                            status: false,
                            message: `Failed, order id ${dataOrderItem[i].id} can't remove package due to msisdn number ${dataOrderItem[i].msisdn} hasn't activated package id ${dataOrderItem[i].package_id}`,
                            data: null,
                        })
                        return
                    }
                    //validate package before add
                    if(existancePackage && dataOrderItem[i].order_type == "New") {
                        res.status(422).json({
                            status: false,
                            message: `Failed, order id ${dataOrderItem[i].id} can't add package due to msisdn number ${dataOrderItem[i].msisdn} already activated package id ${dataOrderItem[i].package_id}`,
                            data: null,
                        })
                        return
                    }
                    if(existancePackage && dataOrderItem[i].order_type == "Remove") {
                        validOrderItemToRmvAdd.push(dataOrderItem[i])
                    }
                    if(!existancePackage && dataOrderItem[i].order_type == "New") {
                        validOrderItemToRmvAdd.push(dataOrderItem[i])
                    }
                }
            }
            var updateOrderItemCnclRmvAdd = []
            
            //cancel order item
            for(let i = 0; i < validOrderItemToCancel.length; i++) {
                await order_item.update(
                    {status: "canceled"},
                    {where: {id: {[Op.eq]: validOrderItemToCancel[i].id} } }
                )
                await order_item.findOne(
                    {where: {id: {[Op.eq]: validOrderItemToCancel[i].id} } }
                )
                .then(async data => {
                    updateOrderItemCnclRmvAdd.push(data.dataValues)
                })
            }
            
            //complete remove and add packages
            for(let i = 0; i < validOrderItemToRmvAdd.length; i++) {
                //remove package
                if(validOrderItemToRmvAdd[i].order_type == "Remove") {
                    await msisdn_package.update(
                        {
                            renewal: false,
                            is_active: false,
                            expire_date: Date.now()
                        },
                        {   
                            where: {
                                [Op.and]: [
                                    { package_id: {[Op.eq]: validOrderItemToRmvAdd[i].package_id} },
                                    { msisdn: {[Op.eq]: validOrderItemToRmvAdd[i].msisdn} }
                                ]
                            }
                        }
                    )
                }
                //add package
                if(validOrderItemToRmvAdd[i].order_type == "New") {
                    await msisdn_package.create(
                        {
                            msisdn: validOrderItemToRmvAdd[i].msisdn,
                            package_id: validOrderItemToRmvAdd[i].package_id,
                            renewal: true,
                            is_active: true,
                            start_date: Date.now(),
                            expire_date: '9999-01-01 07:00:00'
                        },
                        {   
                            where: {
                                [Op.and]: [
                                    { package_id: {[Op.eq]: validOrderItemToRmvAdd[i].package_id} },
                                    { msisdn: {[Op.eq]: validOrderItemToRmvAdd[i].msisdn }  }
                                ]
                            }
                        }
                    )
                }
                await order_item.update(
                    {status: "completed"},
                    {
                        where: {
                            [Op.and]: [
                                { package_id: { [Op.eq]: validOrderItemToRmvAdd[i].package_id } },
                                { msisdn: { [Op.eq]: validOrderItemToRmvAdd[i].msisdn } },
                                { status: { [Op.eq]: "open"}}
                            ]
                        }
                    }
                )
            }

            // let updateDataOrderItem = []
            for(let i = 0; i < validOrderItemToRmvAdd.length; i++) {    
                await order_item.findOne(
                    {where: {id: {[Op.eq]: validOrderItemToRmvAdd[i].id} } }
                )
                .then(async data => {
                    updateOrderItemCnclRmvAdd.push(data.dataValues)
                })
            }

            res.status(200).json({
                status: true,
                message: "Success! change status order item",
                data: updateOrderItemCnclRmvAdd
            })

        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: error.message,
                data: error
            })
        }
    }

}

module.exports = orderController