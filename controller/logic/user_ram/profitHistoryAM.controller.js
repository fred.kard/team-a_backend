const { account_manager } = require('../../../models');
const { corporate_account } = require('../../../models')
const { billing } = require('../../../models');
const db = require('../../../models/index');
const { QueryTypes } = require('sequelize');

const {
    hashSync,
    genSaltSync,
    compareSync
} = require("bcrypt");

class profitHistoryAM{

    //feature untuk menampilkan profit history 
    async profitHistory(req,res,next){
        try {
            //mengambil database dari paramas req.email
            var account_am = req.email

            //mengecek email dari table account_manager
            const AmId = await account_manager.findOne({
                where: {
                    email : account_am.email
                }
            })    
            if (!AmId) {
                res.status(404).json({
                    status: false,
                    message: 'AM not found',
                    data: null,
                })
            }
            try {
                //query dari table billings
                const queryHistory = ("SELECT billings.month, billings.year, sum(billings.total_invoice) as profit from billings left join corporate_accounts on corporate_accounts.account_id = billings.account_id  LEFT JOIN account_managers on corporate_accounts.am_id = account_managers.am_id where corporate_accounts.am_id = '"+ AmId.am_id +"' group by billings.month,billings.year order by billings.year,billings.month;")
                const data = await db.sequelize.query(queryHistory, { type: QueryTypes.SELECT })
                
                if (!data) {
                    res.status(422).json({
                        status: false,
                        message: 'data profit not found',
                        data: null,
                    })
                }
                res.status(200).json({
                    status: true,
                    message: "data profit retrieved",
                    data
                })

            }
            catch(err) {
                res.status(500).json({
                    status: false,
                    message: err.message
                })
            }
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message
            })
        }
    }    
}

module.exports = profitHistoryAM

                









