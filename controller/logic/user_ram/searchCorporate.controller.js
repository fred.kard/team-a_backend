const Sequelize = require('sequelize');
const { QueryTypes } = require('sequelize');
const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const { account_manager } = require('../../../models');

class corporateController {
    async searchCorps(req, res) {
        try {        
        const data = await sequelize.query(querygetSearchByAccount_name, { type: QueryTypes.SELECT });
        if (data === null) {
            res.status(204).json({
                status: false,
                message: 'data not found',
                data: null,
            })
        }
        else {
            let listcorporate = [];
            var foto = data[0].dataValues.logo
            const buffer = Buffer.from(foto);
            for(let index = data.length - 1; index >= 0; --index) 
                {
                    listcorporate[index] = {
                        account_id: data[index].account_id,
                        account_name: data[index].account_name,
                        address: data[index].address,
                        line_of_business: data[index].line_of_business,
                        phone: data[index].phone,
                        am_id: data[index].am_id,
                        pic_id: data[index].pic_id,
                        virtual_account: data[index].virtual_account,
                        logo: buffer.toString(),    
                    }
                }
                
                res.status(200).json({
                    status: true,
                    message: `movies with keyword search ${req.body.account_name} retrieved!`,
                    data: {
                        page: 1,
                        nextPage: false,
                        count: data.length,
                        data: listcorporate,
                    }
                })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message,
            data: null,
        })
    }
    }
}

module.exports = corporateController
