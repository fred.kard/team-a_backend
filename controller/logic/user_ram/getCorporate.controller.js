const { pic_corporate_account } = require('../../../models');
const { corporate_account } = require('../../../models');
const { account_manager } = require('../../../models');


class corporateAMController {
    async myCorporate(req, res, next) {
        try {
            var user_am = req.email
            const id_am = await account_manager.findAll({
                where: {
                    email: user_am.email
                }
            })

            // Mengambil all data pada tabel corporate account
            await corporate_account.findAll({
                where: {
                    am_id: id_am[0].dataValues.am_id
                }

            }).then(async data => {
                if (data.length == 0){
                    res.status(200).json({
                        status: false,
                        message: "you dont have any Corporate Member",
                        data: null,
                    })
                } else {
                    var listcorporate = []
                    var index = data.length - 1
                    for (index; index >= 0; --index) {
                        // Mengambil data pada pic corporate account, sesuai dengan email PIC
                        const name = await pic_corporate_account.findAll({
                            where: {
                                pic_id: data[index].pic_id
                            }
                        })
                       
                        // Mengubah output avatar dari bentuk buffer menjadi string (menggunakan BLOB)
                        var foto = name[0].dataValues.avatar
                        const buffer = Buffer.from(foto);
                        console.log(buffer)

                        listcorporate[index] = {
                            account_id: data[index].account_id,
                            account_name: data[index].account_name,
                            am_id: data[index].am_id,
                            name: name[0].name,
                            phone: name[0].phone,
                            email: name[0].email,
                            virtual_account: data[index].virtual_account,
                            avatar: buffer.toString(),
                        }
                    }
                    
                    res.status(200).json({
                        status: true,
                        message: "List Corporate Account",
                        data: listcorporate,
                    })
                    next()
                    return
                }
            }).catch((err) => {
                throw new Error(err.toString());
            })
        }
        catch (error) {
            console.log(error)
            const bodyRes = {
                status: false,
                message: "Get Corporate Error",
                data: null
            }
            res.send(bodyRes)
            return
        }
    }
}

module.exports = corporateAMController