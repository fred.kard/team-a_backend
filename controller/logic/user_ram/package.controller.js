//controller untuk merubah package customer yang telah di order
const { 
    msisdn, 
    corporate_account, 
    account_manager, 
    pic_corporate_account, 
    msisdn_package, 
    packages, 
    order_item, 
    order } = require('../../../models');
const { Op } = require('sequelize');

class packageControllerAM {
    async changePackage(req, res, next) {
        try{
            console.log('masuk change package')
            var requestbody = req.body
            console.log(requestbody.length)
            var listNumberChange = []

            //pengecekan user active dan relasi msisdn terhadap AM login
            if (!requestbody[0].account_id) {
                res.status(422).json({
                    status: false,
                    message: 'Please insert account id first',
                    data: null,
                })
                return
            }
            const userActive = req.email;
            const userAm = await account_manager.findOne({
                where: {
                    email: userActive.email
                }
            })
            const userPic = await corporate_account.findOne({
                where: {
                    am_id: userAm.am_id
                }
            })

            //validasi msisdn dan package sebelum di lakukan perubahan
           
            // 1. validasi msisdn
            var index = requestbody.length - 1
            for (index; index > 0; --index){
                const cekMsisdn = await msisdn.findOne({
                where: {
                        msisdn: requestbody[index].msisdn
                }    
                })
                if (!cekMsisdn) {
                    res.status(422).json({
                        status: false,
                        message: `MSISDN ${requestbody[index].msisdn} not belong to account_id`,
                        data: null,
                    })
                    return
                }
                var jumlah_paket_remove = requestbody[index].package_remove.length - 1
                var jumlah_paket_add = requestbody[index].package_new.length - 1
                
                //2. validasi paket sebelum di remove
                for (jumlah_paket_remove; jumlah_paket_remove >=  0; --jumlah_paket_remove){
                const cekPaketRemove = await msisdn_package.findOne({
                    where: {
                        [Op.and]: [
                            { package_id: { [Op.eq]: requestbody[index].package_remove[jumlah_paket_remove].package_id }},
                            { msisdn: { [Op.eq]: requestbody[index].msisdn }}
                        ]
                    }
                })

                    //kondisi paket tidak bisa remove jika sebelumnya memang tidak ada
                    if(!cekPaketRemove){
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_remove[jumlah_paket_remove].package_id} at MSISDN ${requestbody[index].msisdn} not listed, cannot removed`,
                            data: null,
                        })
                        return 
                    }
                    const cekPaketRemoveNotActive = await msisdn_package.findOne({
                        where: {
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index].package_remove[jumlah_paket_remove].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index].msisdn } },
                                { is_active: { [Op.eq]: false } }
                            ]
                        }
                    })

                    //kondisi paket tidak bisa di remove jika sebelumnya telah di remove, ditandai dengan is_active = false
                    if (cekPaketRemoveNotActive) {
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_remove[jumlah_paket_remove].package_id} at MSISDN ${requestbody[index].msisdn} already removed, cannot removed again`,
                            data: null,
                        })
                        return
                    }
                }

                //3. validasi paket sebelum di add
                for (jumlah_paket_add; jumlah_paket_add >= 0; --jumlah_paket_add){
                    const cekPaketAdd = await msisdn_package.findOne({
                        where: {
                            [Op.and]: [
                            {package_id: { [Op.eq]: requestbody[index].package_new[jumlah_paket_add].package_id }},
                            { msisdn: { [Op.eq]: requestbody[index].msisdn } },
                            { is_active: { [Op.eq]: true } }
                            ]
                        }
                    })

                    //kondisi paket masih aktif sehingga tidak bisa ditambah ulang
                    if(cekPaketAdd){
                        res.status(422).json({
                            status: false,
                            message: `Package_id ${requestbody[index].package_new[jumlah_paket_add].package_id} at MSISDN ${requestbody[index].msisdn} already listed, cannot duplicated`,
                            data: null,
                        })
                        return  
                    }
                }
            }

            //jika semua telah melewati filtering maka akan dibuatkan order_id
            var create_order = await order.create({
                am_id: userAm.am_id,
                account_id: requestbody[0].account_id,
                pic_id: userPic.pic_id,
                status: "completed",
                order_type: "Change Packages",
                order_qty: requestbody.length-1
            })
            var index2 = requestbody.length - 1

            //looping untuk mengetahui jumlah msisdn yang berubah paket
            for (index2; index2 > 0; --index2){
                var jumlah_paket_remove2 = requestbody[index2].package_remove.length-1
                var jumlah_paket_add2 = requestbody[index2].package_new.length-1
                var listPackage = []

                //upate db order item untuk paket remove
                for (jumlah_paket_remove2; jumlah_paket_remove2 >= 0; --jumlah_paket_remove2){
                    await order_item.create({
                        order_id: create_order.order_id,
                        msisdn: requestbody[index2].msisdn,
                        package_id: requestbody[index2].package_remove[jumlah_paket_remove2].package_id,
                        renewal: false,
                        status: "completed",
                        order_type: "Remove"
                    }).then(async data => {
                        listPackage.push({
                            'package_id': data.dataValues.package_id,
                            'order_type':"Remove",
                        })
                    })
                    //update msisdn package untuk package remove
                    await msisdn_package.update({
                        renewal: false,
                        is_active: false,
                        expire_date: Date.now()},
                        {
                            where: {
                                [Op.and]: [
                                    { package_id: { [Op.eq]: requestbody[index2].package_remove[jumlah_paket_remove2].package_id } },
                                    { msisdn: { [Op.eq]: requestbody[index2].msisdn } }
                                ]
                            }
                        }
                    )
                }

                //update db order item untuk paket add
                for (jumlah_paket_add2; jumlah_paket_add2 >= 0; --jumlah_paket_add2){
                    await order_item.create({
                        order_id: create_order.order_id,
                        msisdn: requestbody[index2].msisdn,
                        package_id: requestbody[index2].package_new[jumlah_paket_add2].package_id,
                        renewal: true,
                        status: "completed",
                        order_type: "New"
                    }).then(async data => {
                        listPackage.push({
                            'package_id': data.dataValues.package_id,
                            'order_type': "New",
                        })
                    })

                    //jika sebelumnya pernah beli paket, maka cukup di update active renewal,start_date, expire_date
                    const cekPaketAddRenewal = await msisdn_package.findOne({
                        where:{
                            [Op.and]: [
                                { package_id: { [Op.eq]: requestbody[index2].package_new[jumlah_paket_add2].package_id } },
                                { msisdn: { [Op.eq]: requestbody[index2].msisdn } }
                            ]
                        }
                    })
                    if (cekPaketAddRenewal){
                        await msisdn_package.update({
                            renewal: true,
                            is_active: true,
                            start_date: Date.now(),
                            expire_date: '9999-01-01 07:00:00'
                        },
                            {
                                where: {
                                    [Op.and]: [
                                        { package_id: { [Op.eq]: requestbody[index2].package_new[jumlah_paket_add2].package_id } },
                                        { msisdn: { [Op.eq]: requestbody[index2].msisdn } }
                                    ]
                                }
                            }
                        )
                    } else{
                    // jika sebelumnya belum pernah beli paket, maka akan dibuatkan row baru di msisdn_package
                    await msisdn_package.create({
                        msisdn: requestbody[index2].msisdn,
                        package_id: requestbody[index2].package_new[jumlah_paket_add2].package_id,
                        renewal: true,
                        is_active: true,
                        start_date: Date.now(),
                        expire_date: '9999-01-01 07:00:00'
                    })
                    }
                }
                listNumberChange.push({
                    "msisdn": requestbody[index2].msisdn,
                    "package": listPackage
                })
            }
            res.status(201).json({
                status: true,
                message: "Your changes has been saved!",
                data: listNumberChange
            })
        } catch (error) {
            console.log(error)
            res.status(500).json({
                status: false,
                message: error.message,
                data: null
            })
        }
    }
}

module.exports = packageControllerAM