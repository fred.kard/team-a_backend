const { msisdn, corporate_account, account_manager, pic_corporate_account, msisdn_package, packages, order } = require('../../../models');
const {Op} = require('sequelize');
const msisdnValidator = require('../../http/middleware/msisdnValidation');


class MsisdnController {
    async addMsisdn(req,res,next) {
        try {
            const userAccountId = req.body.account_id

            const userAm = req.email
            const amEmail = userAm.email
            
            const dataAccount = await corporate_account.findOne({
                where: {account_id: userAccountId}
            })
            if (!dataAccount) {
                res.status(422).json({
                    status: false,
                    message: `Failed, account id ${userAccountId} not found`,
                    data: userAccountId
                })
                return
            }

            const dataAm = await account_manager.findOne({
                where: { [Op.and] : [ 
                    {email: {[Op.eq]:amEmail}},
                    {am_id: {[Op.eq]:dataAccount.am_id}}
                ]}
            })

            if(!dataAm) {
                res.status(422).json({
                    status: false,
                    message: `Failed, account id ${userAccountId} does not belong to you`,
                    data: userAccountId
                })
                return
            }          

            const userMsisdn = msisdnValidator.msisdnValidator(req.body.msisdn)
            console.log(`test check ${userMsisdn}`)
            
            switch(userMsisdn) {
                case `msisdn not a number` : 
                res.status(422).json({
                    status: false,
                    message: `msidn ${req.body.msisdn} not a number`,
                    data: userMsisdn
                }) 
                break;
                case `please check your first number` : 
                res.status(422).json({
                    status: false,
                    message: `msidn ${req.body.msisdn} wrong first number`,
                    data: userMsisdn
                }) 
                break;
                case `wrong country code` : 
                res.status(422).json({
                    status: false,
                    message: `msidn ${req.body.msisdn} wrong country code`,
                    data: userMsisdn
                }) 
                break;
                case `not a phone number` : 
                res.status(422).json({
                    status: false,
                    message: `msidn ${req.body.msisdn} not a phone number`,
                    data: userMsisdn
                }) 
                break;
                case `not a Telkomsel msisdn` : 
                res.status(422).json({
                    status: false,
                    message: `msidn ${req.body.msisdn} not a Telkomsel number`,
                    data: userMsisdn
                }) 
                break;
                case 'check your length number' : 
                res.status(422).json({
                    status: false,
                    message: `msidn ${req.body.msisdn} length not match`,
                    data: userMsisdn
                }) 
                break;
                case userMsisdn : 

                    const dataMsisdn = await msisdn.findOne({
                        where: {msisdn: userMsisdn}
                    })

                    if(dataMsisdn) {
                        res.status(422).json({
                            status: false,
                            message: `msidn ${userMsisdn} already exsist`,
                            data: dataMsisdn
                        })
                        return
                    }

                    const addMsisdn = await msisdn.create({
                        msisdn: userMsisdn,
                        full_name: req.body.full_name,
                        imsi: req.body.imsi || null,
                        iccid: req.body.iccid || null,
                        puk_1: req.body.puk_1|| null,
                        puk_2: req.body.puk_2 || null,
                        account_id: req.body.account_id,
                        is_active: true,
                    })
                    
                    if(addMsisdn) {
                        res.status(200).json({
                            status: true,
                            message: `Success! add a number ${userMsisdn} to account id ${userAccountId}`,
                            data: addMsisdn
                        })
                    }
                break;
                // default:
            }
        } catch(error) {
            res.status(500).json ({
                status: false,
                message: error.message,
                data: error
            })
        }
    }

    async addMsisdnByAm(req,res,next){
        try{
            const userActive = req.email
            const userActiveEmail = userActive.email
            
            const dataUserActiveRaw = await account_manager.findOne({where: {email: userActiveEmail}})
            const dataUserActive = dataUserActiveRaw.dataValues
            console.log(dataUserActive)
            const reqAccountId = req.body.account_id;
            const reqQtyAddMsisdn = req.body.numbers.quantity
            const reqPackage = req.body.numbers.package_id
            if(
                reqAccountId === undefined || reqAccountId == "" || isNaN(reqAccountId) ||
                reqQtyAddMsisdn === undefined || reqQtyAddMsisdn == "" || isNaN(reqQtyAddMsisdn) ||
                reqPackage === undefined || reqPackage == "" || isNaN(reqPackage)
            ) {
                res.status(422).json({
                    status: false,
                    message: `Falied, check your input`,
                    data: null
                })
            }

            const dataAccountRaw = await corporate_account.findOne({where: {account_id: reqAccountId}})
            //error handling order id not found
            if(!dataAccountRaw) {
                res.status(422).json ({
                    status: false,
                    message: `Falied, account id ${reqAccountId} not found`,
                    data: null
                })
            } 
            const dataAccount = dataAccountRaw.dataValues
            console.log(dataAccount)
            const dataPackageRaw = await packages.findOne({where: {package_id: reqPackage}})
            //error handling package id not found
            if(!dataPackageRaw) {
                res.status(422).json ({
                            status: false,
                            message: `Falied, package id ${reqPackage} not found`,
                            data: null
                        })
            }
            
            //error account id not belong to user
            if(dataAccount.am_id != dataUserActive.am_id) {
                res.status(422).json ({
                    status: false,
                    message: `Falied, sorry account id ${reqAccountId} not belong to you`,
                    data: null
                })
            }

            //check exsisting msisdn
            let exsistingMsisdnArray = []
            const existingMsisdnRaw = await msisdn.findAll({where: {account_id: reqAccountId}})
            for (let i = 0; i < existingMsisdnRaw.length; i++) {
                exsistingMsisdnArray.push(existingMsisdnRaw[i].dataValues.msisdn)
            }
            
            //generate data msisdn & makes sure theres no duplicate msisdn 
            let newDataMsisdnArray = []
            let newDataMsisdn ;
            let newMsisdnArray = []
            let newMsisdn;

            function generateDataMsisdn() {
                newMsisdn = '628110' + reqAccountId + Math.floor(1000 + Math.random() * 9000)
                if(checkExsisting(newMsisdn) && checkGeneratedMsisdn(newMsisdn)) {
                    newMsisdnArray.push(newMsisdn);
                    newDataMsisdn = {
                        msisdn: newMsisdn,
                        full_name   : 'Karyawan perusahaan id ' + reqAccountId,
                        imsi        : '5101054110' + reqAccountId + newMsisdn[8] + newMsisdn[9] + newMsisdn[10],
                        iccid       : '985467283418764' + Math.floor(1000 + Math.random() * 9000),
                        puk_1       : '35958' + Math.floor( Math.random()*999 ),
                        puk_2       : '95802' + Math.floor( Math.random()*999 ),
                        account_id  : +reqAccountId,
                        is_active   : true
                    }
                    newDataMsisdnArray.push(newDataMsisdn)
                return newMsisdn
                }
                return generateDataMsisdn()
            }

            function checkGeneratedMsisdn(el) {
                for( let i = 0; i < newMsisdnArray.length; i++) 
                    if(newMsisdnArray[i] == el) return false;
                return true;
            }

            function checkExsisting(el) {
                for (let i = 0; i < exsistingMsisdnArray.length; i++) 
                    if(exsistingMsisdnArray[i] == el) return false;
                return true;
            }

            for( let i = 0; i < reqQtyAddMsisdn; i++ ) {
                generateDataMsisdn()
            }

            let resData = []
            for (let i = 0; i < newDataMsisdnArray.length; i++ ) {
                await msisdn.create({
                    msisdn    : newDataMsisdnArray[i].msisdn,
                    full_name : newDataMsisdnArray[i].full_name,
                    imsi      : newDataMsisdnArray[i].imsi,
                    iccid     : newDataMsisdnArray[i].iccid,
                    puk_1     : newDataMsisdnArray[i].puk_1,
                    puk_2     : newDataMsisdnArray[i].puk_2,
                    account_id: newDataMsisdnArray[i].account_id,
                    is_active : newDataMsisdnArray[i].is_active,
                })
                await msisdn_package.create({
                    msisdn: newDataMsisdnArray[i].msisdn,
                    package_id: reqPackage,
                    renewal: true,
                    is_active: true,
                    start_date: Date.now(),
                    expire_date: '9999-01-01 07:00:00'
                })
                resData.push({
                    msisdn: newMsisdnArray[i],
                    package: reqPackage
                })
            }

            //auto create order after AM add numbers 
            await order.create({
                am_id: dataUserActive.am_id,
                account_id: reqAccountId,
                pic_id: dataAccount.pic_id,
                status: "completed",
                order_type: "Add Numbers",
                order_qty: reqQtyAddMsisdn,
                package_id_add_number: reqPackage
            })

            res.status(201).json({
                status: true,
                message: `Success! add ${reqQtyAddMsisdn} numbers to account id ${reqAccountId}`,
                data: resData
            })
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: error.message,
                data: error
            })
        }
    }

    async getMsisdn(req,res,next) {
        try {
            const reqAccountId = req.query
            if(reqAccountId === undefined || reqAccountId == "") {
                res.status(422).json({
                    status: false,
                    message: `Falied, check your input`,
                    data: null
                })
            }

            const dataCorpAccount = await corporate_account.findOne({where : {account_id: reqAccountId.account_id} })
            //error handling account id not found
            if(!dataCorpAccount) {
                res.status(422).json({
                    status: false,
                    message: `Falied, account id ${reqAccountId.account_id} not found`,
                    data: null,
                })
            }
            const dataCorp = dataCorpAccount.dataValues

            
            const userActive = req.email;
            const userDomain = userActive.email.split("@")[1]
            //error handling order items not belong to user
            if(userDomain == "telkomsel.co.id") {
                let userAm = await account_manager.findOne({where: {email: userActive.email} })
                if(userAm.dataValues.am_id != dataCorp.am_id) {
                    res.status(422).json({
                        status: false,
                        message: `Falied, account id ${reqAccountId.account_id} not belong to you`,
                        data: null,
                    })
                }
            }
            else {
                let userPic = await pic_corporate_account.findOne({where: {email: userActive.email} })
                if(userPic.dataValues.pic_id != dataCorp.pic_id) {
                    res.status(422).json({
                        status: false,
                        message: `Falied, account id ${reqAccountId.account_id} not belong to you`,
                        data: null,
                    })
                }
            }            

            let data = await msisdn.findAll({
                attributes: ['msisdn'],
                where: {
                    [Op.and]: [
                        {account_id: reqAccountId.account_id},
                        {is_active: true}
                    ]
                },
                include: {
                    model: msisdn_package,
                    as: 'msisdn_has_packages',
                    is_active: true,
                    include: {
                        model: packages,
                        as: 'package_in_relasi_packages'
                    }
                }
            })
            let countMsisdn = data.length

            function joinPackages(length,i){
                var listPackage = [] 
                for (let j=0; j<length; j++) {
                    listPackage.push({
                        'package_id': data[i].dataValues.msisdn_has_packages[j].package_id,
                        'package_name': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_name,
                        'package_type': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_type,
                        'package_value': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_value
                    })
                }
                return listPackage
            }
            
            let listMsisdnAndPackage = []

            for(let i = 0; i < countMsisdn; i++) {
                let length = data[i].dataValues.msisdn_has_packages.length
                listMsisdnAndPackage.push({
                    'msisdn': data[i].dataValues.msisdn,
                    'packages': joinPackages(length,i)
                })
            }
            res.status(200).json ({
                status: true,
                message: `Success! get all number of ${dataCorp.account_name} account id ${reqAccountId.account_id}`,
                data: listMsisdnAndPackage
            })
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: error.message,
                data: error
            })
            
        }
    }

    async getMsisdnAm(req,res,next) {
        try {
            const reqAccountId = req.query
            if(reqAccountId === undefined || reqAccountId == "") {
                res.status(422).json({
                    status: false,
                    message: `Falied, check your input`,
                    data: null
                })
            }

            const dataCorpAccount = await corporate_account.findOne({where : {account_id: reqAccountId.account_id} })
            //error handling account id not found
            if(!dataCorpAccount) {
                res.status(422).json({
                    status: false,
                    message: `Falied, account id ${reqAccountId.account_id} not found`,
                    data: null,
                })
            }
            const dataCorp = dataCorpAccount.dataValues
            
            const userActive = req.email;
            let userAm = await account_manager.findOne({where: {email: userActive.email} })
            if(userAm.dataValues.am_id != dataCorp.am_id) {
                res.status(422).json({
                    status: false,
                    message: `Falied, account id ${reqAccountId.account_id} not belong to you`,
                    data: null,
                })
            }        

            let data = await msisdn.findAll({
                attributes: ['msisdn'],
                where: {
                    [Op.and]: [
                        {account_id: reqAccountId.account_id},
                        {is_active: true}
                    ]
                },
                include: {
                    model: msisdn_package,
                    as: 'msisdn_has_packages',
                    is_active: true,
                    include: {
                        model: packages,
                        as: 'package_in_relasi_packages'
                    }
                }
            })
            let countMsisdn = data.length

            function joinPackages(length,i){
                var listPackage = [] 
                for (let j=0; j<length; j++) {
                    listPackage.push({
                        'package_id': data[i].dataValues.msisdn_has_packages[j].package_id,
                        'package_name': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_name,
                        'package_type': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_type,
                        'package_value': data[i].dataValues.msisdn_has_packages[j].package_in_relasi_packages.dataValues.package_value
                    })
                }
                return listPackage
            }
            
            let listMsisdnAndPackage = []

            for(let i = 0; i < countMsisdn; i++) {
                let length = data[i].dataValues.msisdn_has_packages.length
                listMsisdnAndPackage.push({
                    'msisdn': data[i].dataValues.msisdn,
                    'packages': joinPackages(length,i)
                })
            }
            res.status(200).json ({
                status: true,
                message: `Success! get all number of ${dataCorp.account_name} account id ${reqAccountId.account_id}`,
                data: listMsisdnAndPackage
            })
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: error.message,
                data: error
            })
            
        }
    }
}

module.exports = MsisdnController

