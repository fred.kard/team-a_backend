const { corporate_account } = require('../../../models');
const { account_manager } = require('../../../models');
const { billing } = require('../../../models');
const sequelize = require('sequelize')

class RamBillingController {

    async billingList (req, res) {
    
        try {
            const userActive = req.email;
        
        const foundAccManager = await account_manager.findOne({
                where:{
                    email: userActive.email
                }
        })
        if (foundAccManager !== null){
            const foundCorpAccount = await corporate_account.findAll({  
                where :{
                    am_id:foundAccManager.am_id
                    },
                raw: true,
                nest: true,
            })
            let totalBilling= []
            
            for (let i = 0; i < foundCorpAccount.length; i++)
            if(foundCorpAccount[i].length !== 0) {
                totalBilling[i] = await billing.findAll({
                    attributes: [
    
                      'account_id',
                      [sequelize.fn('sum', sequelize.col('total_invoice')), 'total_invoice'],
                      [sequelize.fn('sum', sequelize.col('total_outstanding')), 'total_outstanding'],
                                                   
                    ],
                    group: ['account_id'],
                    where :{
                        account_id:foundCorpAccount[i].account_id
                        },
                    raw: true,
                    nest: true,
                    
                  });
            }
            let dataOutput =[]
            for (let i = 0; i < totalBilling.length; i++){
                for (let j = 0; j < totalBilling[i].length; j++) {
                        dataOutput[i] = {
                        account_name : foundCorpAccount[i].account_name,
                        account_id : totalBilling[i][j].account_id,
                        total_invoice : totalBilling[i][j].total_invoice,
                        total_outstanding : totalBilling[i][j].total_outstanding,
                    }
                }
                
            }
    
            res.status(200).json({
                status : 'true',
                message : 'Billing of Corporate Account retrieved ',
                data : dataOutput
            })
            
        }
    
        else {
            res.status(422).json({
                status: false,
                message: 'User not authorized',
                data: null,
            })
        }
    
        }
        catch(err) {
            res.status(500).json({
                status: false,
                message: err.message,
                data: null,
        })
        }
    
    }
    
    async unpaidBillingList (req, res) {
        try {
            const userActive = req.email;
        
            const foundAccManager = await account_manager.findOne({
                where:{
                    email: userActive.email
                }
            })
            if (foundAccManager !== null){
                const foundCorpAccount = await corporate_account.findAll({  
                    where :{
                        am_id:foundAccManager.am_id
                    },
                    raw: true,
                    nest: true,
                })
               

                if(foundCorpAccount.length !== 0) {
            
                    let listUnpaidBills =[]
                    for (let i = 0; i < foundCorpAccount.length; i++) {
                        if(foundCorpAccount[i].length !== 0) {
                            let cekUnpaidBills= await billing.findAll({
                                where :{
                                    [sequelize.Op.and]: [
                                    { account_id: foundCorpAccount[i].account_id },
                                    { status_payment: "UNPAID" }
                                    ]
                                },                    
                            });


                            // ensure output listUnpaidBills does not contain empty array

                            if( cekUnpaidBills.length !== 0 ){
                                let foundUnpaidBills= await billing.findAll({
                                    where :{
                                        [sequelize.Op.and]: [
                                        { account_id: foundCorpAccount[i].account_id },
                                        { status_payment: "UNPAID" }
                                        ]
                                    },                    
                                });
                                listUnpaidBills.push(foundUnpaidBills);
                            }                                                      
                            
                        }
                    }
                
            res.status(200).json({
                status : 'true',
                message : 'Unpaid Bills of corporate_account ',
                data : listUnpaidBills
            }) 
        }
         else {
                
                res.status(422).json({
                        status: false,
                        message: 'Your am_account is not related to any corporate account',
                        data: null,
                })
                
        }
      
        } else {
            res.status(422).json({
                status: false,
                message: 'Your account is not authorized',
                data: null
        })
        
    }
    
    }
    catch(err) {
        res.status(500).json({
            status: false,
            message: err.message,
            data: null,
        })
    }
    
    }    

}


    
module.exports = RamBillingController    
    

