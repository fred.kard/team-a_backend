'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('corporate_accounts', 
    [
      {
        account_name: 'PT Prodia Widyahusada Tbk',
        address:'Jakarta',
        line_of_business:'Healthcare',
        phone:6214121129,
        pic_id:19,
        am_id:1,
        virtual_account: '122341111129',
        logo:'aaaaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT Kimia Farma Tbk',
        address:'Jakarta',
        line_of_business:'Healtcare',
        phone:6215121130,
        pic_id:20,
        am_id:2,
        virtual_account: '122341111130',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('corporate_accounts', null, {});
  }
};
