'use strict';
const bcrypt = require('bcrypt');
const pass = 'Test1234'
const saltRounds = 10;
const passdefault = bcrypt.hashSync(pass, saltRounds);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('pic_corporate_accounts', 
    [
      {
        name: 'Agus Prodia',
        password: passdefault,
        nik: '317519',
        email: 'agus@prodia.co.id',
        phone: '628118601129',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Bayu Kimia Farma',
        password: passdefault,
        nik: '317520',
        email: 'bayu@kimiafarma.co.id',
        phone: '628111881130',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('pic_corporate_accounts', null, {});
  }
};