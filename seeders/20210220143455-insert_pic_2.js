'use strict';
const bcrypt = require('bcrypt');
const pass = 'Test1234'
const saltRounds = 10;
const passdefault = bcrypt.hashSync(pass, saltRounds);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('pic_corporate_accounts', 
    [
      {
        name: 'User Demo PIC 1',
        password: passdefault,
        nik: '317915',
        email: 'user_demo_pic_1@account_1.co.id',
        phone: '628118609211',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'User Demo PIC 2',
        password: passdefault,
        nik: '317522',
        email: 'user_demo_pic_2@account_2.co.id',
        phone: '628118601921',
        job_position: 'IT Operation Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('pic_corporate_accounts', null, {});
  }
};