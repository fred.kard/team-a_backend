'use strict';
const bcrypt = require('bcrypt');
const pass = 'Test1234'
const saltRounds = 10;
const passdefault = bcrypt.hashSync(pass, saltRounds);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('account_managers', 
    [
      {
        name: 'Fajri Rahmanda',
        password: passdefault,
        nik: '84219',
        email: 'fajri_rahmanda@telkomsel.co.id',
        phone: '628118606211',
        job_position: 'Enterprise Mobility Solution Sales Engineering',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Asep Jajang Nurjaya',
        password: passdefault,
        nik: '90189',
        email: 'asep_j_nurjaya@telkomsel.co.id',
        phone: '628111882052',
        job_position: 'Radio, Transport and Power Operation Banda Aceh',
        workplace: 'Banda Aceh',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Dona Saputra',
        password: passdefault,
        nik: '82229',
        email: 'dona_saputra@telkomsel.co.id',
        phone: '62811225544',
        job_position: 'Engineer Consumer SQA Sumbagsel',
        workplace: 'Sumbagsel',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Fajar Wahyu Ardianto',
        password: passdefault,
        nik: '95108',
        email: 'fajar_w_ardianto@telkomsel.co.id',
        phone: '628118119268',
        job_position: 'Radio, Transport and Power Operation Kupang',
        workplace: 'Kupang',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Stephanus Krisna Arizona Widiputra',
        password: passdefault,
        nik: '88014',
        email: 'stephanus_ka_widiputra@telkomsel.co.id',
        phone: '628114907888',
        job_position: 'Radio, Transport and Power Operation Kediri',
        workplace: 'Kediri',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Handoko Darmawan Wicaksono',
        password: passdefault,
        nik: '78102',
        email: 'handoko_d_wicaksono@telkomsel.co.id',
        phone: '62811718777',
        job_position: 'Engineer RAN Deployment West',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Azka Hanif Imtiyaz',
        password: passdefault,
        nik: '96009',
        email: 'azka_h_imtiyaz@telkomsel.co.id',
        phone: '628111113668',
        job_position: 'Corporate Software Development',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Furqon Ahkamy',
        password: passdefault,
        nik: '84099',
        email: 'furqon_ahkamy@telkomsel.co.id',
        phone: '62811646446',
        job_position: 'Engineer Data Communication Engineering',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Imam Abdul Hakim',
        password: passdefault,
        nik: '95036',
        email: 'imam_a_hakim@telkomsel.co.id',
        phone: '628119561222',
        job_position: 'IT Support - Business Jabotabek',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Rizal Antori',
        password: passdefault,
        nik: '82345',
        email: 'rizal_antori@telkomsel.co.id',
        phone: '628117308118',
        job_position: 'Radio, Transport and Power Operation Pringsewu',
        workplace: 'Lampung',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('account_managers', null, {});
  }
};
