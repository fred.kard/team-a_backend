'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('packages',
   [
     {
       package_name : 'Flash Corporate 20K',
       package_value : '500 MB',
       package_type : 'data',
       product_status : 'in_active',
       package_id : 1,
       price : 20000,
       updatedAt: new Date(),
       createdAt : new Date()

     }, 
     {
      package_name : 'Flash Corporate 25K',
      package_value : '1 GB',
      package_type : 'data',
      product_status : 'in_active',
      package_id : 2,
      price : 25000,
      updatedAt: new Date(),
      createdAt : new Date()
     },
     {
        package_name : 'Flash Corporate 60K',
        package_value : '3 GB',
        package_type : 'data',
        product_status : 'in_active',
        package_id : 3,
        price : 60000,
        updatedAt: new Date(),
        createdAt : new Date()     
     },
     {
      package_name : 'Flash Corporate 75K',
      package_value : '5 GB',
      package_type : 'data',
      product_status : 'in_active',
      package_id : 4,
      price : 75000,
      updatedAt: new Date(),
      createdAt : new Date()     
   },
   {
    package_name : 'Flash Corporate 120K',
    package_value : '8 GB',
    package_type : 'data',
    product_status : 'in_active',
    package_id : 5,
    price : 120000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'Flash Corporate 150K',
    package_value : '12 GB',
    package_type : 'data',
    product_status : 'in_active',
    package_id : 6,
    price : 150000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'Flash Corporate 250K',
    package_value : '22 GB',
    package_type : 'data',
    product_status : 'in_active',
    package_id : 7,
    price : 250000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'Flash Corporate 350K',
    package_value : '32 GB',
    package_type : 'data',
    product_status : 'in_active',
    package_id : 8,
    price : 350000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'Flash Corporate 500K',
    package_value : '50 GB',
    package_type : 'data',
    product_status : 'in_active',
    package_id : 9,
    price : 500000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'NSP Corporate',
    package_value : '2 NSP corporate',
    package_type : 'nsp',
    product_status : 'in_active',
    package_id : 10,
    price : 5000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'NSP Corporate',
    package_value : '1 NSP Corporate',
    package_type : 'nsp',
    product_status : 'in_active',
    package_id : 11,
    price : 3000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'TeamPlan 50K',
    package_value : 'data 2 GB, 1000 mins voice, 1000 sms',
    package_type : 'TeamPlan',
    product_status : 'in_active',
    package_id : 12,
    price : 50000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'TeamPlan 100K',
    package_value : 'data 2 GB, 1000 mins voice, 1000 sms',
    package_type : 'TeamPlan',
    product_status : 'in_active',
    package_id : 13,
    price : 100000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'TeamPlan 200K',
    package_value : 'data 2 GB, 1000 mins voice, 1000 sms',
    package_type : 'data',
    product_status : 'in_active',
    package_id : 14,
    price : 200000,
    updatedAt: new Date(),
    createdAt : new Date()     
   },
   {
    package_name : 'TeamPlan 300K',
    package_value : 'data 2 GB, 1000 mins voice, 1000 sms',
    package_type : 'data',
    product_status : 'in_active',
    package_id : 15,
    price : 300000,
    updatedAt: new Date(),
    createdAt : new Date()     
   }
   ]
   
   
   )

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('packages', null, {});
  }
};
