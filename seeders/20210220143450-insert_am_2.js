'use strict';
const bcrypt = require('bcrypt');
const pass = 'Test1234'
const saltRounds = 10;
const passdefault = bcrypt.hashSync(pass, saltRounds);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('account_managers', 
    [
      {
        name: 'User Demo AM',
        password: passdefault,
        nik: '99219',
        email: 'user_demo_am@telkomsel.co.id',
        phone: '628118601162',
        job_position: 'Software Developer',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('account_managers', null, {});
  }
};
