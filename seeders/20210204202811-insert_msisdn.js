'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('msisdns',
   [
    {
      msisdn: "628110185678",
      full_name: "Tony",
      imsi: "510105411018567",
      iccid: "9854672834187643275",
      puk_1: "35958021",
      puk_2: "95802135",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185679",
      full_name: "Budi",
      imsi: "510105411018568",
      iccid: "9854672834187643276",
      puk_1: "35958022",
      puk_2: "95802136",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185680",
      full_name: "Agus",
      imsi: "510105411018569",
      iccid: "9854672834187643275",
      puk_1: "35958023",
      puk_2: "95802137",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185681",
      full_name: "Firman",
      imsi: "510105411018570",
      iccid: "9854672834187643276",
      puk_1: "35958024",
      puk_2: "95802138",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185682",
      full_name: "Ilham",
      imsi: "510105411018571",
      iccid: "9854672834187643275",
      puk_1: "35958025",
      puk_2: "95802139",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185683",
      full_name: "Tika",
      imsi: "510105411018572",
      iccid: "9854672834187643276",
      puk_1: "35958026",
      puk_2: "95802140",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185684",
      full_name: "Sarah",
      imsi: "510105411018573",
      iccid: "9854672834187643275",
      puk_1: "35958027",
      puk_2: "95802141",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185685",
      full_name: "Dani",
      imsi: "510105411018574",
      iccid: "9854672834187643276",
      puk_1: "35958028",
      puk_2: "95802142",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110185686",
      full_name: "Erik",
      imsi: "510105411018575",
      iccid: "9854672834187643275",
      puk_1: "35958029",
      puk_2: "95802143",
      account_id: 18,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175687",
      full_name: "Steve",
      imsi: "510105411017576",
      iccid: "9854672834187643276",
      puk_1: "35958030",
      puk_2: "95802144",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175688",
      full_name: "Roy",
      imsi: "510105411017577",
      iccid: "9854672834187643275",
      puk_1: "35958031",
      puk_2: "95802145",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175689",
      full_name: "Tono",
      imsi: "510105411017578",
      iccid: "9854672834187643276",
      puk_1: "35958032",
      puk_2: "95802146",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175690",
      full_name: "Ria",
      imsi: "510105411017579",
      iccid: "9854672834187643275",
      puk_1: "35958033",
      puk_2: "95802147",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175691",
      full_name: "Vira",
      imsi: "510105411017580",
      iccid: "9854672834187643276",
      puk_1: "35958034",
      puk_2: "95802148",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175692",
      full_name: "Fitri",
      imsi: "510105411017581",
      iccid: "9854672834187643275",
      puk_1: "35958035",
      puk_2: "95802149",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175693",
      full_name: "Deny",
      imsi: "510105411017582",
      iccid: "9854672834187643276",
      puk_1: "35958036",
      puk_2: "95802150",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175694",
      full_name: "Ari",
      imsi: "510105411017583",
      iccid: "9854672834187643275",
      puk_1: "35958037",
      puk_2: "95802151",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110175695",
      full_name: "Widi",
      imsi: "510105411017584",
      iccid: "9854672834187643276",
      puk_1: "35958038",
      puk_2: "95802152",
      account_id: 17,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155696",
      full_name: "Jono",
      imsi: "510105411015585",
      iccid: "9854672834187643275",
      puk_1: "35958039",
      puk_2: "95802153",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155697",
      full_name: "Heri",
      imsi: "510105411015586",
      iccid: "9854672834187643276",
      puk_1: "35958040",
      puk_2: "95802154",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155698",
      full_name: "Tya",
      imsi: "510105411015587",
      iccid: "9854672834187643275",
      puk_1: "35958041",
      puk_2: "95802155",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155699",
      full_name: "Bram",
      imsi: "510105411015588",
      iccid: "9854672834187643276",
      puk_1: "35958042",
      puk_2: "95802156",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155700",
      full_name: "Cika",
      imsi: "510105411015589",
      iccid: "9854672834187643275",
      puk_1: "35958043",
      puk_2: "95802157",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155701",
      full_name: "Monik",
      imsi: "510105411015590",
      iccid: "9854672834187643276",
      puk_1: "35958044",
      puk_2: "95802158",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155702",
      full_name: "Wendy",
      imsi: "510105411015591",
      iccid: "9854672834187643275",
      puk_1: "35958045",
      puk_2: "95802159",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155703",
      full_name: "Hendry",
      imsi: "510105411015592",
      iccid: "9854672834187643276",
      puk_1: "35958046",
      puk_2: "95802160",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155704",
      full_name: "Vero",
      imsi: "510105411015593",
      iccid: "9854672834187643275",
      puk_1: "35958047",
      puk_2: "95802161",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110155705",
      full_name: "Tommy",
      imsi: "510105411015594",
      iccid: "9854672834187643276",
      puk_1: "35958048",
      puk_2: "95802162",
      account_id: 15,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135706",
      full_name: "Dody",
      imsi: "510105411013595",
      iccid: "9854672834187643275",
      puk_1: "35958049",
      puk_2: "95802163",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135707",
      full_name: "Tony",
      imsi: "510105411013596",
      iccid: "9854672834187643276",
      puk_1: "35958050",
      puk_2: "95802164",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135708",
      full_name: "Budi",
      imsi: "510105411013597",
      iccid: "9854672834187643275",
      puk_1: "35958051",
      puk_2: "95802165",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135709",
      full_name: "Agus",
      imsi: "510105411013598",
      iccid: "9854672834187643276",
      puk_1: "35958052",
      puk_2: "95802166",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135710",
      full_name: "Firman",
      imsi: "510105411013599",
      iccid: "9854672834187643275",
      puk_1: "35958053",
      puk_2: "95802167",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135711",
      full_name: "Ilham",
      imsi: "510105411013600",
      iccid: "9854672834187643276",
      puk_1: "35958054",
      puk_2: "95802168",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135712",
      full_name: "Tika",
      imsi: "510105411013601",
      iccid: "9854672834187643275",
      puk_1: "35958055",
      puk_2: "95802169",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110135713",
      full_name: "Sarah",
      imsi: "510105411013602",
      iccid: "9854672834187643276",
      puk_1: "35958056",
      puk_2: "95802170",
      account_id: 13,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115714",
      full_name: "Dani",
      imsi: "510105411011603",
      iccid: "9854672834187643275",
      puk_1: "35958057",
      puk_2: "95802171",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115715",
      full_name: "Erik",
      imsi: "510105411011604",
      iccid: "9854672834187643276",
      puk_1: "35958058",
      puk_2: "95802172",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115716",
      full_name: "Steve",
      imsi: "510105411011605",
      iccid: "9854672834187643275",
      puk_1: "35958059",
      puk_2: "95802173",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115717",
      full_name: "Roy",
      imsi: "510105411011606",
      iccid: "9854672834187643276",
      puk_1: "35958060",
      puk_2: "95802174",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115718",
      full_name: "Tono",
      imsi: "510105411011607",
      iccid: "9854672834187643275",
      puk_1: "35958061",
      puk_2: "95802175",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115719",
      full_name: "Ria",
      imsi: "510105411011608",
      iccid: "9854672834187643276",
      puk_1: "35958062",
      puk_2: "95802176",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115720",
      full_name: "Vira",
      imsi: "510105411011609",
      iccid: "9854672834187643275",
      puk_1: "35958063",
      puk_2: "95802177",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115721",
      full_name: "Fitri",
      imsi: "510105411011610",
      iccid: "9854672834187643276",
      puk_1: "35958064",
      puk_2: "95802178",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115722",
      full_name: "Deny",
      imsi: "510105411011611",
      iccid: "9854672834187643275",
      puk_1: "35958065",
      puk_2: "95802179",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115723",
      full_name: "Ari",
      imsi: "510105411011612",
      iccid: "9854672834187643276",
      puk_1: "35958066",
      puk_2: "95802180",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115724",
      full_name: "Widi",
      imsi: "510105411011613",
      iccid: "9854672834187643275",
      puk_1: "35958067",
      puk_2: "95802181",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115725",
      full_name: "Jono",
      imsi: "510105411011614",
      iccid: "9854672834187643276",
      puk_1: "35958068",
      puk_2: "95802182",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115726",
      full_name: "Heri",
      imsi: "510105411011615",
      iccid: "9854672834187643275",
      puk_1: "35958069",
      puk_2: "95802183",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110115727",
      full_name: "Tya",
      imsi: "510105411011616",
      iccid: "9854672834187643276",
      puk_1: "35958070",
      puk_2: "95802184",
      account_id: 11,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110095728",
      full_name: "Bram",
      imsi: "510105411009617",
      iccid: "9854672834187643275",
      puk_1: "35958071",
      puk_2: "95802185",
      account_id: 9,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110095729",
      full_name: "Cika",
      imsi: "510105411009618",
      iccid: "9854672834187643276",
      puk_1: "35958072",
      puk_2: "95802186",
      account_id: 9,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110095730",
      full_name: "Monik",
      imsi: "510105411009619",
      iccid: "9854672834187643275",
      puk_1: "35958073",
      puk_2: "95802187",
      account_id: 9,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110095731",
      full_name: "Wendy",
      imsi: "510105411009620",
      iccid: "9854672834187643276",
      puk_1: "35958074",
      puk_2: "95802188",
      account_id: 9,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110095732",
      full_name: "Hendry",
      imsi: "510105411009621",
      iccid: "9854672834187643275",
      puk_1: "35958075",
      puk_2: "95802189",
      account_id: 9,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110095733",
      full_name: "Vero",
      imsi: "510105411009622",
      iccid: "9854672834187643276",
      puk_1: "35958076",
      puk_2: "95802190",
      account_id: 9,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110095734",
      full_name: "Tommy",
      imsi: "510105411009623",
      iccid: "9854672834187643275",
      puk_1: "35958077",
      puk_2: "95802191",
      account_id: 9,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075735",
      full_name: "Dody",
      imsi: "510105411007624",
      iccid: "9854672834187643276",
      puk_1: "35958078",
      puk_2: "95802192",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075736",
      full_name: "Dody",
      imsi: "510105411007625",
      iccid: "9854672834187643275",
      puk_1: "35958079",
      puk_2: "95802193",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075737",
      full_name: "Tony",
      imsi: "510105411007626",
      iccid: "9854672834187643276",
      puk_1: "35958080",
      puk_2: "95802194",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075738",
      full_name: "Budi",
      imsi: "510105411007627",
      iccid: "9854672834187643275",
      puk_1: "35958081",
      puk_2: "95802195",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075739",
      full_name: "Agus",
      imsi: "510105411007628",
      iccid: "9854672834187643276",
      puk_1: "35958082",
      puk_2: "95802196",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075740",
      full_name: "Firman",
      imsi: "510105411007629",
      iccid: "9854672834187643275",
      puk_1: "35958083",
      puk_2: "95802197",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075741",
      full_name: "Ilham",
      imsi: "510105411007630",
      iccid: "9854672834187643276",
      puk_1: "35958084",
      puk_2: "95802198",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075742",
      full_name: "Tika",
      imsi: "510105411007631",
      iccid: "9854672834187643275",
      puk_1: "35958085",
      puk_2: "95802199",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075743",
      full_name: "Sarah",
      imsi: "510105411007632",
      iccid: "9854672834187643276",
      puk_1: "35958086",
      puk_2: "95802200",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075744",
      full_name: "Dani",
      imsi: "510105411007633",
      iccid: "9854672834187643275",
      puk_1: "35958087",
      puk_2: "95802201",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110075745",
      full_name: "Erik",
      imsi: "510105411007634",
      iccid: "9854672834187643276",
      puk_1: "35958088",
      puk_2: "95802202",
      account_id: 7,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055746",
      full_name: "Steve",
      imsi: "510105411005635",
      iccid: "9854672834187643275",
      puk_1: "35958089",
      puk_2: "95802203",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055747",
      full_name: "Roy",
      imsi: "510105411005636",
      iccid: "9854672834187643276",
      puk_1: "35958090",
      puk_2: "95802204",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055748",
      full_name: "Tono",
      imsi: "510105411005637",
      iccid: "9854672834187643275",
      puk_1: "35958091",
      puk_2: "95802205",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055749",
      full_name: "Ria",
      imsi: "510105411005638",
      iccid: "9854672834187643276",
      puk_1: "35958092",
      puk_2: "95802206",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055750",
      full_name: "Vira",
      imsi: "510105411005639",
      iccid: "9854672834187643275",
      puk_1: "35958093",
      puk_2: "95802207",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055751",
      full_name: "Fitri",
      imsi: "510105411005640",
      iccid: "9854672834187643276",
      puk_1: "35958094",
      puk_2: "95802208",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055752",
      full_name: "Deny",
      imsi: "510105411005641",
      iccid: "9854672834187643275",
      puk_1: "35958095",
      puk_2: "95802209",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055753",
      full_name: "Ari",
      imsi: "510105411005642",
      iccid: "9854672834187643276",
      puk_1: "35958096",
      puk_2: "95802210",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110055754",
      full_name: "Tika",
      imsi: "510105411005643",
      iccid: "9854672834187643275",
      puk_1: "35958097",
      puk_2: "95802211",
      account_id: 5,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035755",
      full_name: "Sarah",
      imsi: "510105411003644",
      iccid: "9854672834187643276",
      puk_1: "35958098",
      puk_2: "95802212",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035756",
      full_name: "Dani",
      imsi: "510105411003645",
      iccid: "9854672834187643275",
      puk_1: "35958099",
      puk_2: "95802213",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035757",
      full_name: "Erik",
      imsi: "510105411003646",
      iccid: "9854672834187643276",
      puk_1: "35958100",
      puk_2: "95802214",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035758",
      full_name: "Steve",
      imsi: "510105411003647",
      iccid: "9854672834187643275",
      puk_1: "35958101",
      puk_2: "95802215",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035759",
      full_name: "Roy",
      imsi: "510105411003648",
      iccid: "9854672834187643276",
      puk_1: "35958102",
      puk_2: "95802216",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035760",
      full_name: "Tono",
      imsi: "510105411003649",
      iccid: "9854672834187643275",
      puk_1: "35958103",
      puk_2: "95802217",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035761",
      full_name: "Ria",
      imsi: "510105411003650",
      iccid: "9854672834187643276",
      puk_1: "35958104",
      puk_2: "95802218",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035762",
      full_name: "Vira",
      imsi: "510105411003651",
      iccid: "9854672834187643275",
      puk_1: "35958105",
      puk_2: "95802219",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035763",
      full_name: "Fitri",
      imsi: "510105411003652",
      iccid: "9854672834187643276",
      puk_1: "35958106",
      puk_2: "95802220",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110035764",
      full_name: "Deny",
      imsi: "510105411003653",
      iccid: "9854672834187643275",
      puk_1: "35958107",
      puk_2: "95802221",
      account_id: 3,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015765",
      full_name: "Ari",
      imsi: "510105411001654",
      iccid: "9854672834187643276",
      puk_1: "35958108",
      puk_2: "95802222",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015766",
      full_name: "Widi",
      imsi: "510105411001655",
      iccid: "9854672834187643275",
      puk_1: "35958109",
      puk_2: "95802223",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015767",
      full_name: "Ria",
      imsi: "510105411001656",
      iccid: "9854672834187643276",
      puk_1: "35958110",
      puk_2: "95802224",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015768",
      full_name: "Vira",
      imsi: "510105411001657",
      iccid: "9854672834187643275",
      puk_1: "35958111",
      puk_2: "95802225",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015769",
      full_name: "Fitri",
      imsi: "510105411001658",
      iccid: "9854672834187643276",
      puk_1: "35958112",
      puk_2: "95802226",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015770",
      full_name: "Deny",
      imsi: "510105411001659",
      iccid: "9854672834187643275",
      puk_1: "35958113",
      puk_2: "95802227",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015771",
      full_name: "Ari",
      imsi: "510105411001660",
      iccid: "9854672834187643276",
      puk_1: "35958114",
      puk_2: "95802228",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015772",
      full_name: "Widi",
      imsi: "510105411001661",
      iccid: "9854672834187643275",
      puk_1: "35958115",
      puk_2: "95802229",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015773",
      full_name: "Jono",
      imsi: "510105411001662",
      iccid: "9854672834187643276",
      puk_1: "35958116",
      puk_2: "95802230",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015774",
      full_name: "Heri",
      imsi: "510105411001663",
      iccid: "9854672834187643275",
      puk_1: "35958117",
      puk_2: "95802231",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    },
    {
      msisdn: "628110015775",
      full_name: "Tya",
      imsi: "510105411001664",
      iccid: "9854672834187643276",
      puk_1: "35958118",
      puk_2: "95802232",
      account_id: 1,
      is_active: true,
      updatedAt: new Date(),
      createdAt: new Date(),
    }
   ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('msisdns', null, {});
  }
};
