#!/bin/bash

date_start=`date '+%Y-%m-01'`
date_expired=`date --date='1 month ago' +%Y-%m-01`

last_month=`date --date='1 month ago' +%m`
year_last_month=`date --date='1 month ago' +%Y`

query_select_billings="
select msisdns.account_id, sum(packages.price) as total, sum(packages.price) as outstanding , '$last_month' as for_month , '$year_last_month' as for_year , corporate_accounts.v
irtual_account , 'UNPAID' as status_payment , '$date_start' as createdAt, '$date_start' as updateAt
from msisdn_packages
left join packages on msisdn_packages.package_id = packages.package_id
left join msisdns on msisdn_packages.msisdn = msisdns.msisdn
left join corporate_accounts on msisdns.account_id = corporate_accounts.account_id
where msisdn_packages.start_date < '$date_start' and expire_date >= '$date_expired'
group by msisdns.account_id,corporate_accounts.virtual_account"

query_update_billings="
insert into billings (\"account_id\",\"total_invoice\",\"total_outstanding\",\"month\",\"year\",\"virtual_account\",\"status_payment\",\"createdAt\",\"updatedAt\")
select msisdns.account_id, sum(packages.price) as total, sum(packages.price) as outstanding , '$last_month' as for_month , '$year_last_month' as for_year , corporate_accounts.v
irtual_account , 'UNPAID' as status_payment , '$date_start' as createdAt, '$date_start' as updateAt
from msisdn_packages
left join packages on msisdn_packages.package_id = packages.package_id
left join msisdns on msisdn_packages.msisdn = msisdns.msisdn
left join corporate_accounts on msisdns.account_id = corporate_accounts.account_id
where msisdn_packages.start_date < '$date_start' and expire_date >= '$date_expired'
group by msisdns.account_id,corporate_accounts.virtual_account"

query_select_billing_details="
select billings.billing_id, msisdn_packages.package_id, count(msisdn_packages.package_id) as qty, sum(packages.price) as total ,'$date_start' as createdAt, '$date_start' as upd
ateAt
from msisdn_packages
left join packages on msisdn_packages.package_id = packages.package_id
left join msisdns on msisdn_packages.msisdn = msisdns.msisdn
left join corporate_accounts on msisdns.account_id = corporate_accounts.account_id
left join billings on billings.account_id = corporate_accounts.account_id
where msisdn_packages.start_date < '$date_start' and expire_date >= '$date_expired' and billings.month = '$last_month' and billings.year = '$year_last_month'
and \"billings\".\"createdAt\" = '$date_start'
group by billings.billing_id,msisdn_packages.package_id"

query_update_billing_details="
insert into billing_details (\"billing_id\",\"package_id\",\"package_qty\",\"package_total\",\"createdAt\",\"updatedAt\")
select billings.billing_id, msisdn_packages.package_id, count(msisdn_packages.package_id) as qty, sum(packages.price) as total ,'$date_start' as createdAt, '$date_start' as upd
ateAt
from msisdn_packages
left join packages on msisdn_packages.package_id = packages.package_id
left join msisdns on msisdn_packages.msisdn = msisdns.msisdn
left join corporate_accounts on msisdns.account_id = corporate_accounts.account_id
left join billings on billings.account_id = corporate_accounts.account_id
where msisdn_packages.start_date < '$date_start' and expire_date >= '$date_expired' and billings.month = '$last_month' and billings.year = '$year_last_month'
and \"billings\".\"createdAt\" = '$date_start'
group by billings.billing_id,msisdn_packages.package_id"

echo " "
echo " "
echo "Generate Billing and billing detail Start "
echo `date`
echo " "
echo "data billing"
echo $query_select_billings  | docker exec -i binar-postgresql psql -U teama -d teama_dev
echo " "
echo " "
echo $query_update_billings | docker exec -i binar-postgresql psql -U teama -d teama_dev
echo " "
echo "data billing details"
echo $query_select_billing_details | docker exec -i binar-postgresql psql -U teama -d teama_dev
echo " "
echo " "
echo $query_update_billing_details | docker exec -i binar-postgresql psql -U teama -d teama_dev
echo " "
echo " "
echo "Generate Billing and billing detail End "
echo "========================"
echo " "
echo " "
