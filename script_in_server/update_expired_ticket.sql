update "public"."table_tickets" set 
"status" = 'close',
"action" = 'Ticket Auto Close after 24 Hours from updated date' where "ticket_id" in
(select table_tickets.ticket_id
from table_tickets 
where ("table_tickets"."updatedAt" + interval '1 DAY') <= CURRENT_DATE  and "table_tickets"."status" <> 'close' ) ;