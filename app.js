var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const CORSMiddleware = require('./controller/http/middleware/CORS.middleware');
var billingRouter = require ('./routes/billing')
var chatRouter = require ('./routes/chat')

const env = require('dotenv');
env.config();

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('json spaces', 2)
app.use(bodyParser.json())

app.use(logger('dev'));
app.use(express.json({limit:'50mb'}));
app.use(CORSMiddleware);
app.use(express.urlencoded({
    limit: "50mb", extended: true, parameterLimit:5000000}
));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use('/', indexRouter);
app.use('/api', usersRouter);
app.use('/api', billingRouter);
app.use('/api', chatRouter);

module.exports = app;
