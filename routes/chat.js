var express = require('express');
var router = express.Router();
var path = require('path');
const { apikeyMiddleware } = require('../controller/http/middleware/apikey.middleware');
const { JWTMiddleware } = require('../controller/http/middleware/JWT.middleware');

const chatController = require('../controller/logic/chatting/chat.controller')
console.log('masuk chat router')
const cc = new chatController
router.post('/sendChat', apikeyMiddleware, JWTMiddleware, cc.sendChat)
router.use(express.static(path.join(__dirname, 'public')));

//API untuk mengakses chatting
router.get('/chat', (req, res) => {
    console.log('masuk ke chat router get')
    res.render('index', {username: req.query.username , room: req.query.account_id})
})

module.exports = router;