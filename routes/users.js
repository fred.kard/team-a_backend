var express = require('express');
var router = express.Router();
const { apikeyMiddleware } = require('../controller/http/middleware/apikey.middleware');
const { JWTMiddleware } = require('../controller/http/middleware/JWT.middleware');

const authController = require('../controller/logic/auth/auth.controller')
const ticketController = require('../controller/logic/user_corporate/ticket.controller')
const packageController = require('../controller/logic/user_corporate/package.controller')
const ticketAMController = require('../controller/logic/user_ram/ticketAM.controller')
const corporateAMController = require('../controller/logic/user_ram/getCorporate.controller')
const editprofileAMController = require('../controller/logic/user_ram/editProfileRam.controller')
const editprofileCorporateController = require('../controller/logic/user_corporate/editProfileCorporate.controller')
const corporateController = require('../controller/logic/user_ram/searchCorporate.controller')
const billCorporate = require('../controller/logic/user_corporate/billCorporate.controller')
const MsisdnAmController = require('../controller/logic/user_ram/msisdnAm.controller')
const MsisdnPicController = require('../controller/logic/user_corporate/msisdnPic.controller')
const OrderController = require('../controller/logic/user_corporate/order.controller')
const AMOrderController = require('../controller/logic/user_ram/ram_order.controller')
const profitHistoryAM = require('../controller/logic/user_ram/profitHistoryAM.controller');
const billMonthController = require('../controller/logic/user_corporate/billMonth.controller');
const billDetail = require('../controller/logic/user_corporate/billDetail.controller');
const packageControllerAM = require ('../controller/logic/user_ram/package.controller')
const CurrentpackageSummaryControllerPIC = require ('../controller/logic/user_corporate/currentPackageSummary.controller')
const PaymentControllerPIC = require ('../controller/logic/user_corporate/payment.controller')
const ticketPICcontroller = require('../controller/logic/user_corporate/ticketPIC.controller')

const AuthController = new authController
const TicketController = new ticketController
const PackageController = new packageController
const TicketAMController = new ticketAMController
const CorporateAMController = new corporateAMController
const EditProfileAMController = new editprofileAMController
const EditProfileCorporateController = new editprofileCorporateController
const CorporateController = new corporateController
const BillCorporate = new billCorporate
const AmMsisdnController = new MsisdnAmController
const PicMsisdnController = new MsisdnPicController
const orderController = new OrderController
const AMorderController = new AMOrderController
const ProfitHistoryAM = new profitHistoryAM
const billmonthController = new billMonthController
const billDetailController = new billDetail
const changePackage = new packageControllerAM
const currentpackageSummaryControllerPIC = new CurrentpackageSummaryControllerPIC
const paymentControllerPIC = new PaymentControllerPIC
const TicketPICController = new ticketPICcontroller

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});




// ======================================== unused API ========================================
//Login Corporate (unused)
router.post('/logincorporate', apikeyMiddleware, AuthController.authCorporate)
//login AM (unused)
router.post('/logintelkomsel', apikeyMiddleware, AuthController.authAM)
//Add msisdn for AM 
router.post('/addMsisdn', apikeyMiddleware, JWTMiddleware, AmMsisdnController.addMsisdn)
//create order for PIC corp tidak dipakai
router.post('/createOrderAddPackages', apikeyMiddleware, JWTMiddleware, orderController.orderAddPackages)
//get all order items by am_id for AM (jika dibutuhkan saja) 
router.get('/getMyOrderItemsByAMID', apikeyMiddleware, JWTMiddleware, AMorderController.getMyOrderItemsByAMID)
//get all order for PIC corp
router.get('/getAllMyCorpOrderItems', apikeyMiddleware, JWTMiddleware, orderController.getMyOrder)
//Get list of msisdn based on Account_id for PIC
router.get('/getMsisdnPic', apikeyMiddleware, JWTMiddleware, PicMsisdnController.getMsisdnPic)
//Get list of msisdn based on Account_id for AM
router.get('/getMsisdnAm', apikeyMiddleware, JWTMiddleware, AmMsisdnController.getMsisdnAm)



// ======================================== Login & Edit Profile ========================================
//login for AM or Corporate User
router.post('/login', apikeyMiddleware, AuthController.auth)
//Edit profile AM
router.post('/editprofileram', JWTMiddleware, apikeyMiddleware, EditProfileAMController.editProfileAM)
//Edit profile PIC Corporate
router.post('/editprofilepic', JWTMiddleware, apikeyMiddleware, EditProfileCorporateController.editProfileCorporate)





// ======================================== Ticket function ========================================

// Ticket for PIC ========================================
// Create Ticket 
router.post('/createTicket', apikeyMiddleware, JWTMiddleware, TicketController.createTicket)
// Get Ticket 
router.get('/ticketuser', apikeyMiddleware, JWTMiddleware, TicketController.viewUserTicket)
// Get Ticket detail by Ticket Id for PIC
router.get('/myticketdetailpic', apikeyMiddleware, JWTMiddleware, TicketPICController.myTicketDetailPIC )
// Post Ticket comment
router.post('/replyticketpic', apikeyMiddleware, JWTMiddleware, TicketPICController.replyComplainPIC)
// Close Ticket 
router.put('/closeticket', apikeyMiddleware, JWTMiddleware, TicketController.closeTicket)

// Ticket for AM ========================================
// Get Ticket 
router.get('/myTicket', apikeyMiddleware, JWTMiddleware, TicketAMController.myTicket)
// Get Ticket detail by Ticket ID
router.get('/myticketdetailam', apikeyMiddleware, JWTMiddleware, TicketAMController.myTicketDetailAM)
// Post Ticket comment
router.post('/replyticketam', apikeyMiddleware, JWTMiddleware, TicketAMController.replyComplainAM)




// ======================================== Corporate function ========================================

// Get list Corporate handled by AM
router.get('/myCorporate', apikeyMiddleware, JWTMiddleware, CorporateAMController.myCorporate)





// ======================================== order function ========================================

// order function belongs to PIC ========================================

//get all order for AM
router.get('/getOrderForPIC', apikeyMiddleware, JWTMiddleware, orderController.getOrderForPIC)

//get order Item based on order id For AM
router.get('/getOrderItemsforPIC/:order_id', apikeyMiddleware, JWTMiddleware, orderController.getOrderItemsforPIC)

//create order add Number for PIC corp
router.post('/createOrderAddNumbers', apikeyMiddleware, JWTMiddleware,  orderController.orderAddNumbers)

// order function belongs to AM ========================================

//get all order for AM
router.get('/getOrderForAM', apikeyMiddleware, JWTMiddleware, AMorderController.getOrderForAM)

//get order Item based on order id For AM
router.get('/getOrderItemsforAM/:order_id', apikeyMiddleware, JWTMiddleware, AMorderController.getOrderItemsforAM)

//change Order status by AM
router.post('/changeStatusOrder', apikeyMiddleware, JWTMiddleware, AMorderController.changeStatusOrder)

//change Order Item status by AM
router.post('/changeStatusOrderItem', apikeyMiddleware, JWTMiddleware, AMorderController.changeStatusOrderItem)






// ======================================== packages, msisdn & msisdn_packages ========================================

//Add multiple msisdn For AM 
router.post('/addMsisdnByAM', apikeyMiddleware, JWTMiddleware, AmMsisdnController.addMsisdnByAm)

//change package by AM
router.post('/changepackage', apikeyMiddleware, JWTMiddleware, changePackage.changePackage)

//change package PIC
router.post('/changePackagePic', apikeyMiddleware, JWTMiddleware, PackageController.changePackagePic)

//Get list of msisdn based on Account_id for AM & PIC
router.get('/getMsisdn', apikeyMiddleware, JWTMiddleware, AmMsisdnController.getMsisdn)

// get list Package Telkomsel for PIC & AM
router.get('/listPackage', apikeyMiddleware, JWTMiddleware, PackageController.listPackage)

//get current package summary by PIC
router.get('/getCurrentPackageSummary', apikeyMiddleware, JWTMiddleware, currentpackageSummaryControllerPIC.currentPackageSummary)





// ======================================== Billing function ========================================

// Billing for AM ========================================
//Get list Billing belong to account for PIC
router.get('/billcorporate', apikeyMiddleware, JWTMiddleware, BillCorporate.searchBill)

//get  Profit History AM
router.get('/profithistory', apikeyMiddleware, JWTMiddleware, ProfitHistoryAM.profitHistory)

// Billing for PIC ========================================
//Get Bill Monthly
router.get('/getBillMonth', apikeyMiddleware, JWTMiddleware, billmonthController.searchBill)

//get detail Bill by billing id
router.get('/getBillDetail', apikeyMiddleware, JWTMiddleware, billDetailController.detailBilling)

//Update status Payment Billing
router.put('/updatePayment', apikeyMiddleware, JWTMiddleware, paymentControllerPIC.updatePayment)



module.exports = router;