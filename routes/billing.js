var express = require('express');
var router = express.Router();
const { apikeyMiddleware } = require('../controller/http/middleware/apikey.middleware');
const { JWTMiddleware } = require('../controller/http/middleware/JWT.middleware');

const RamBillingController = require('../controller/logic/user_ram/ramBilling.controller')
const CorporateBillingController = require('../controller/logic/user_corporate/corporateBillingController')

const corporateBillingController = new CorporateBillingController
const ramBillingController = new RamBillingController

// Get Bills Summary  of Corporate info for AM
router.get('/ramcorpbilling', apikeyMiddleware, JWTMiddleware, ramBillingController.billingList)

// Get Bills Summary of Corporate info for PIC Corporate
router.get('/piccorpbilling', apikeyMiddleware, JWTMiddleware, corporateBillingController.billingList)

//Get Unpaid Bill for AM
router.get('/ramunpaidcorpbilling', apikeyMiddleware, JWTMiddleware, ramBillingController.unpaidBillingList)

//Get Unpaid Bill for PIC Corporate
router.get('/unpaidcorpbilling', apikeyMiddleware, JWTMiddleware, corporateBillingController.unpaidBillingList)


module.exports = router;