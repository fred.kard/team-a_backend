'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn( 
      'billing_details', 
      'billing_id',
      {
       type : Sequelize.INTEGER,
       references: {
         model: 'billings',
         key: 'billing_id',
       }
      });
 
      await queryInterface.changeColumn( 
        'billing_details', 
        'package_id',
        {
         type : Sequelize.INTEGER,
         references: {
           model: 'packages',
           key: 'package_id',
         }
        });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn('billing_details','billing_id',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('billing_details','package_id',{type : Sequelize.INTEGER});
  }
};
