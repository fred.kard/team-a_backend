'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('msisdns', {
      msisdn: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
      },
      full_name: {
        type: Sequelize.STRING
      },
      imsi: {
        type: Sequelize.STRING
      },
      iccid: {
        type: Sequelize.STRING
      },
      puk_1: {
        type: Sequelize.STRING
      },
      puk_2: {
        type: Sequelize.STRING
      },
      account_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      is_active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('msisdns');
  }
};