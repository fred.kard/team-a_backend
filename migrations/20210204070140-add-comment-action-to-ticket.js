'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('table_tickets', 'comment', Sequelize.STRING)
    await queryInterface.addColumn('table_tickets', 'action', Sequelize.STRING)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('table_tickets', 'comment')
    await queryInterface.removeColumn('table_tickets', 'action')
  }
};
