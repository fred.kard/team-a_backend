'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
 
      await queryInterface.changeColumn( 
        'msisdn_packages', 
        'package_id',
        {
         type : Sequelize.INTEGER,
         references: {
           model: 'packages',
           key: 'package_id',
         }
        });
      
      await queryInterface.changeColumn( 
        'msisdn_packages', 
        'msisdn',
        {
          type : Sequelize.STRING,
          references: {
            model: 'msisdns',
            key: 'msisdn',
          }
        });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn('msisdn_packages','package_id',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('msisdn_packages','msisdn',{type : Sequelize.STRING});
  }
};
