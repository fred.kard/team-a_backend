'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn( 
      'billings', 
      'account_id',
      {
       type : Sequelize.INTEGER,
       references: {
         model: 'corporate_accounts',
         key: 'account_id',
       }
      });

    
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn('billings','account_id',{type : Sequelize.INTEGER});
    
  }
};
