'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('billing_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      billing_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      package_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      package_qty: {
        type: Sequelize.INTEGER
      },
      package_total: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('billing_details');
  }
};