'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn( 
      'order_items', 
      'order_id',
      {
       type : Sequelize.INTEGER,
       references: {
         model: 'orders',
         key: 'order_id',
       }
      });
 
      await queryInterface.changeColumn( 
        'order_items', 
        'package_id',
        {
         type : Sequelize.INTEGER,
         references: {
           model: 'packages',
           key: 'package_id',
         }
        });
      
      await queryInterface.changeColumn( 
        'order_items', 
        'msisdn',
        {
          type : Sequelize.STRING,
          references: {
            model: 'msisdns',
            key: 'msisdn',
          }
        });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn('order_items','order_id',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('order_items','package_id',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('order_items','msisdn',{type : Sequelize.STRING});
  }
};
