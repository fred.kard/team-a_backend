'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class pic_corporate_account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.corporate_account, {
        foreignKey: 'pic_id',
        as: 'pic_has_corp'
      });

      this.hasMany(models.table_ticket, {
        foreignKey: 'pic_id',
        as: 'pic_has_ticket'
      })

      this.hasMany(models.order, {
        foreignKey: 'order_id',
        as: 'pic_has_order'
      })
    }
  };
  pic_corporate_account.init({
    pic_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    nik: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    job_position: DataTypes.STRING,
    workplace: DataTypes.STRING,
    avatar: DataTypes.BLOB('long'),
    is_active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'pic_corporate_account',
  });
  return pic_corporate_account;
};