'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class billing extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.billing_detail, {
        foreignKey: 'billing_id',
        as: 'billing_has_billingdetails'
      })

      this.belongsTo(models.corporate_account, {
        foreignKey: 'account_id',
        as: 'account_has_billing'
      });
    }
  };
  billing.init({
    billing_id:  {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    account_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    month: DataTypes.STRING,
    year: DataTypes.STRING,
    total_invoice: DataTypes.INTEGER,
    total_outstanding: DataTypes.INTEGER,
    virtual_account: DataTypes.STRING,
    status_payment: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'billing',
  });
  return billing;
};