'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order_item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.order, {
        foreignKey: 'order_id',
        as: 'order_has_orderitems'
      });

      this.belongsTo(models.packages, {
        foreignKey: 'package_id',
        as: 'package_in_orderitems'
      });
    }
  };
  order_item.init({
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    msisdn: {
      allowNull: false,
      type: DataTypes.STRING
    },
    package_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    renewal: DataTypes.BOOLEAN,
    status: DataTypes.STRING,
    order_type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'order_item',
  });
  return order_item;
};