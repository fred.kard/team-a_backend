'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class packages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.billing_detail, {
        foreignKey: 'package_id',
        as: 'package_in_billingdetails'
      })

      this.hasMany(models.msisdn_package, {
        foreignKey: 'package_id',
        as: 'package_in_relasi_packages'
      })

      this.hasMany(models.order_item, {
        foreignKey: 'package_id',
        as: 'package_in_order_item'
      })
    }
  };
  packages.init({
    package_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    package_type: DataTypes.STRING,
    package_name: DataTypes.STRING,
    package_value: DataTypes.STRING,
    price: DataTypes.INTEGER,
    product_status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'packages',
  });
  return packages;
};