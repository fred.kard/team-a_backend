'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class corporate_account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
     
      this.belongsTo(models.account_manager, {
        foreignKey: 'am_id',
        as: 'corp_belongto_am'
      });

      this.belongsTo(models.pic_corporate_account, {
        foreignKey: 'pic_id',
        as: 'corp_belongto_pic'
      });

      this.hasMany(models.msisdn, {
        foreignKey: 'account_id',
        as: 'corp_hasMany_msisdn'
      });


      this.hasMany(models.order, {
        foreignKey: 'order_id',
        as: 'account_has_order'
      })
      
      this.hasMany(models.table_ticket, {
        foreignKey: 'account_id',
        as: 'corp_has_ticket'
      })
    }
  };
  corporate_account.init({
    account_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    account_name: DataTypes.STRING,
    address: DataTypes.STRING,
    line_of_business: DataTypes.STRING,
    phone: DataTypes.STRING,
    pic_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    am_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    virtual_account: DataTypes.STRING,
    logo: DataTypes.BLOB('long'),
    is_active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'corporate_account',
  });
  return corporate_account;
};