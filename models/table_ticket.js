'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class table_ticket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.account_manager, {
        foreignKey: 'am_id',
        as: 'ticket_has_am'
      });

      this.hasMany(models.pic_corporate_account, {
        foreignKey: 'pic_id',
        as: 'ticket_has_pic'
      });

      this.hasMany(models.corporate_account, {
        foreignKey: 'account_id',
        as: 'ticket_has_corporate'
      });

      this.hasMany(models.ticket_detail, {
        foreignKey: 'ticket_id',
        as: 'ticket_has_detail'
      });
    }
  };
  table_ticket.init({
    ticket_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ticket_category: DataTypes.STRING,
    am_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    account_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    pic_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: DataTypes.STRING,
    comment: DataTypes.STRING,
    action: DataTypes.STRING,
    response: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'table_ticket',
  });
  return table_ticket;
};