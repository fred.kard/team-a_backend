'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class msisdn extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.corporate_account, {
        foreignKey: 'account_id',
        as: 'msisdn_belongto_corp'
      });
      this.hasMany(models.msisdn_package, {
        foreignKey: 'msisdn',
        as: 'msisdn_has_packages'
      });
    }
  };
  msisdn.init({
    msisdn:  {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.STRING,
      autoIncrement: true,
    },
    full_name: DataTypes.STRING,
    imsi: DataTypes.STRING,
    iccid: DataTypes.STRING,
    puk_1: DataTypes.STRING,
    puk_2: DataTypes.STRING,
    account_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    is_active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'msisdn',
  });
  return msisdn;
};