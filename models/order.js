'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.order_item, {
        foreignKey: 'order_id',
        as: 'order_has_orderitems'
      })

      this.belongsTo(models.account_manager, {
        foreignKey: 'am_id',
        as: 'am_has_order'
      });

      this.belongsTo(models.pic_corporate_account, {
        foreignKey: 'pic_id',
        as: 'pic_has_order'
      });

      this.belongsTo(models.corporate_account, {
        foreignKey: 'account_id',
        as: 'account_has_order'
      });
    }
  };
  order.init({
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    am_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    account_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    pic_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: DataTypes.STRING,
    order_type: DataTypes.STRING,
    order_qty: DataTypes.INTEGER,
    package_id_add_number: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'order',
  });
  return order;
};