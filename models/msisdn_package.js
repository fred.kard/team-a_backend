'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class msisdn_package extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.msisdn, {
        foreignKey: 'msisdn',
        as: 'msisdn_in_relasi_packages'
      });

      this.belongsTo(models.packages, {
        foreignKey: 'package_id',
        as: 'package_in_relasi_packages'
      });
    }
  };
  msisdn_package.init({
    msisdn: {
      allowNull: false,
      type: DataTypes.STRING
    },
    package_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    renewal: DataTypes.BOOLEAN,
    is_active:DataTypes.BOOLEAN,
    start_date: DataTypes.DATE,
    expire_date: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'msisdn_package',
  });
  return msisdn_package;
};