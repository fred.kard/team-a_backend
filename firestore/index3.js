var fire = require('./fire')
var db = fire.firestore()

db.settings({
    timestampsInSnapshots: true
})
var doc = db.collection('chatting')

const observer = db.collection('chatting').doc('17').collection('MESSAGES').orderBy('createdAt', 'desc')
    .onSnapshot(querySnapshot => {
        querySnapshot.docChanges().forEach(change => {
            if (change.type === 'added') {
                console.log('New chat: ', change.doc.data());
            }
            if (change.type === 'modified') {
                console.log('Modified chat: ', change.doc.data());
            }
            if (change.type === 'removed') {
                console.log('Removed chat: ', change.doc.data());
            }
        });
    });