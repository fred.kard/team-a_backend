const io = require('socket.io')()
const socketapi = {
    io: io
};

const history = []
const client = []

io.on('connection', socket => {
    socket.on('join-room', function (data) {
        console.log('masuk join')
        console.log(data)
        socket.username = data.username
        socket.room = data.room
        client.push({room : socket.room})
        console.log(client)
        socket.join(socket.room)
        var getRoom = client.find(e => (e.room === socket.room))
        console.log("Room", getRoom)
        if(getRoom){
         for(let i=0;i<history.length;i++) {
             if (history[i].room == getRoom.room) {
                io.to(getRoom.room).emit("chat-history",{username: history[i].username, msg: history[i].msg,currentChatTime:history[i].currentChatTime },socket.username);
             }
         }
        }
    })
    socket.on('chat-message', data => {
        socket.username = data.username
        socket.room = data.room
        socket.msg = data.msg
        socket.currentChatTime = data.currentChatTime
        history.push({room:socket.room, username: socket.username, msg:socket.msg, currentChatTime:socket.currentChatTime})
        console.log(history)
        io.to(socket.room).emit('chat-message', {username: socket.username, msg: socket.msg, currentChatTime:socket.currentChatTime})
    })
})

module.exports = socketapi;