FROM keymetrics/pm2:10-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache \
    bash \
    git \
    curl \
    openssh

MAINTAINER TeamA BE capstone project

RUN mkdir -p /usr/src/telkomsel_TeamA_BE
WORKDIR /usr/src/telkomsel_TeamA_BE

COPY package*.json ./
RUN npm cache clean --force
RUN npm install
COPY . .

EXPOSE 3000

CMD [ "pm2-runtime", "start", "pm2.json", "--env", "production"]